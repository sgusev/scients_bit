%% 
%clear;
%f1 = 45000000;
%f2 = 55000000;
%f0 = 50000000;

f1 = 80e6;
f2 = 120e6;
f0 = 50e6;

J0 = 2*10^-6; 
A0 = 0.1;
%A0 = 0.0170; %0.1/5.8824
%A0 = 0.05; %0.1/5
m = 1;
b = 1/(m*(((1.3806503e-23)*300)/1.60217646e-19));
a2 = J0*(b^2)/2;
a1 = J0*b;
alf = 1;


A_in_N = 1000;
A_in_start = -A0;
A_in_stop = A0;
A_in_step = (A_in_stop - A_in_start)/A_in_N;
nonlin_y = zeros(A_in_N,1);
nonlin_x = zeros(A_in_N,1);

K = 1;
for X = A_in_start:A_in_step:A_in_stop
	nonlin_y(K,1) = J0*(exp(b*(X))-1);
    nonlin_x(K,1) = X;
    K = K + 1;
end

D_N = 100;
D_in_start = 0; %�� ������ 0
D_in_stop = 0.001;
%D_in_stop = 0.001;
%D_in_stop = 0.00025;
D_in_step = D_in_stop/D_N;

steepnees_shift = 2;
step = 0.01*10^-9;
steepnees_sin_out_prfkt_i = (step*(steepnees_shift*2))/(2*abs(sin(2*pi*100e6*step*steepnees_shift)));% T/V [C/B]


size_row = 1+D_N;
I1_f2 	= zeros(size_row,1);
I1_f1 	= zeros(size_row,1);
I2_f2 	= zeros(size_row,1);
I2_f1 	= zeros(size_row,1);

I3_f1 	= zeros(size_row,1);
I3_f2 	= zeros(size_row,1);
I4_f1 	= zeros(size_row,1);
I4_f2 	= zeros(size_row,1);
D_out	= zeros(size_row,1);
D_in_x	= zeros(size_row,1);

%for A0 = 0.0001:0.001:0.1
	K = 1;
	for D_in = D_in_start:D_in_step:D_in_stop
        I1_f2 = (a2^2)*(D_in^2)*alf*(1/(pi^2))*(pi/alf)*atan(pi*(f2-2*f0)/alf);
		I1_f1 = (a2^2)*(D_in^2)*alf*(1/(pi^2))*(pi/alf)*atan(pi*(f1-2*f0)/alf);
		I2_f2 = 2*(a2^2)*(A0^2)*(D_in)*alf*(1/(4*pi^2))*(2*pi/alf)*atan(2*pi*(f2-2*f0)/alf);
		I2_f1 = 2*(a2^2)*(A0^2)*(D_in)*alf*(1/(4*pi^2))*(2*pi/alf)*atan(2*pi*(f1-2*f0)/alf);
	%	D_out(K,1) = (1/8)*((a2)^2)*(A0)^4 + I1_f2 - I1_f1 + I2_f2 - I2_f1;
		
		I3_f1 = (a2^2)*(D_in^2)*2*alf*(1/(pi^2))*(pi/alf)*atan(pi*(f1)/alf);
		I3_f2 = (a2^2)*(D_in^2)*2*alf*(1/(pi^2))*(pi/alf)*atan(pi*(f2)/alf);
		I4_f1 = 2*(a2^2)*(A0^2)*(D_in)*alf*(1/(4*pi^2))*(2*pi/alf)*atan(2*pi*(f1)/alf);
		I4_f2 = 2*(a2^2)*(A0^2)*(D_in)*alf*(1/(4*pi^2))*(2*pi/alf)*atan(2*pi*(f2)/alf);
		
		I5_f2 = 2*(a1^2)*(D_in)*alf*(1/(4*pi^2))*(2*pi/alf)*atan(2*pi*(f2-f0)/alf);
		I5_f1 = 2*(a1^2)*(D_in)*alf*(1/(4*pi^2))*(2*pi/alf)*atan(2*pi*(f1-f0)/alf);
		
		
		D_out(K,1) = 1*(I1_f2 - I1_f1 + I2_f2 - I2_f1 + I3_f2 - I3_f1 + I4_f2 - I4_f1 + I5_f2 - I5_f1);
		D_out_t(K,1) = D_out(K,1) * steepnees_sin_out_prfkt_i;
		D_in_x(K,1) = D_in;
		K = K + 1;
	end;
	
	figure();
	plot(D_in_x,D_out);
	grid on;
    title('��������� X - ������');
	hold on;
	
	figure();
	plot(D_in_x,D_out_t);
	grid on;
	title('��������� X_t - ������');
	
%	figure();
%	plot(nonlin_x,nonlin_y);
%	title('���');
%end;

