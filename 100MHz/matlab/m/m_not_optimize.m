%%
%������ ��������� �������
Fs = 1e+011;  % Sampling Frequency
Fstop1 = 75000000;    % First Stopband Frequency
Fpass1 = 80000000;    % First Passband Frequency
Fpass2 = 120000000;   % Second Passband Frequency
Fstop2 = 125000000;   % Second Stopband Frequency
Astop1 = 60;          % First Stopband Attenuation (dB)
Apass  = 1;           % Passband Ripple (dB)
Astop2 = 60;          % Second Stopband Attenuation (dB)
match  = 'stopband';  % Band to match exactly
h  = fdesign.bandpass(Fstop1, Fpass1, Fpass2, Fstop2, Astop1, Apass, ...
                      Astop2, Fs);
Hd = design(h, 'cheby2', 'MatchExactly', match);

%������ ����
Fs = 1e+011;  % Sampling Frequency
Fstop1 = 40000000;    % First Stopband Frequency
Fpass1 = 45000000;    % First Passband Frequency
Fpass2 = 55000000;   % Second Passband Frequency
Fstop2 = 60000000;   % Second Stopband Frequency
Astop1 = 60;          % First Stopband Attenuation (dB)
Apass  = 1;           % Passband Ripple (dB)
Astop2 = 60;          % Second Stopband Attenuation (dB)
match  = 'stopband';  % Band to match exactly
h  = fdesign.bandpass(Fstop1, Fpass1, Fpass2, Fstop2, Astop1, Apass, ...
                      Astop2, Fs);
Hd_noise = design(h, 'cheby2', 'MatchExactly', match);


U = 1;
%����� ��������� ������������ �������
% 0 ������ ������� c ����������� ��������� ������������ �������� �������
% 1 �����n ������� c ����������� ��������� ������������ ������� ������� 
alg = 3;

%M_start = 00000*10^(-12);
%M_step = 500*10^(-12);
%M_stop = 800000*10^(-12);
%T = 0.00001; %����� �������������
T = 0.0001; %����� �������������
step = 0.01*10^-9; %������� ��������� �������
K_T = T/step;

sin_in = zeros((K_T+1),1);
A0 = 0.1;
%A0 = 0.0170;
K = 1;
	for t = 0:step:T
		sin_in(K,1) = A0*sin(2*pi*50e6*t);
		K = K + 1;
	end

%M_start =0*10^(-4);
%M_step = 0.005*10^(-4);
%M_stop = 10*10^(-4);

N_step = 10;
M_start =0;
M_stop = 0.001;
%M_stop = 0.001;
M_step = (M_stop-M_start)/N_step;


% M_start =0*10^(-4);
% M_step = 0.000005*10^(-4);
% M_stop = 0.010*10^(-4);

	K_Mat = 1;
	N_j = 1;
	steepnees_shift = 2; % ����� ��� �������� ��������
	size_row = (N_step) + 1; %round - ���������� �� ���������� ������
	Mat_sin_out_Y_high 		= zeros(size_row,1);
	Mat_sin_out_Y_diff 	= zeros(size_row,1);
	Mat_sin_out_X 		= zeros(size_row,1);
	Mat_sin_out_N_Z 	= zeros(size_row,1);
	Mat_sin_out_N_Z_diff 	= zeros(size_row,1);
	ampl_noize 			= zeros(size_row,1);
	Disp_sin_out_Y_high 		= zeros(size_row,1);
	Disp_sin_out_Y_diff	= zeros(size_row,1);
	Disp_sin_out_Z 		= zeros(size_row,1);
	Disp_sin_out_X 		= zeros(size_row,1);
	Disp_sin_out_N_Z_high 	= zeros(size_row,1);
	Disp_sin_out_N_Z_diff 	= zeros(size_row,1);
	Disp_sin_out_N_X 	= zeros(size_row,1);
	Disp_sin_out_N_X_t 	= zeros(size_row,1);
	Disp_ampl_noise 	= zeros(size_row,1);
	
	high_point_sin_out_prfkt 			= zeros(10000,1);
	high_point_sin_out					= zeros(10000,1);
	N_high_point_sin_out_prfkt 			= zeros(10000,1);
	zero_point_sin_out_prfkt 			= zeros(10000,1);
	N_zero_point_sin_out_prfkt 			= zeros(10000,1);
	% steepnees_sin_out_prfkt 			= zeros(10000,1);
	diff_high_point_sin_out 			= zeros(10000,1);
	zero_point_sin_out 					= zeros(10000,1);
	diff_zero_point_sin_out 			= zeros(10000,1);
	diff_N_zero_point_sin_out_prfkt 	= zeros(10000,1);

	% ���������� ���������� ���������� �������
	size_row = size(real(sin_in),1);
	nonlin_p_prfkt = zeros(size_row,1);
	nonlin_n_prfkt = zeros(size_row,1);
	nonlin_prfkt = zeros(size_row,1);
	%J0 = 100*10^-6;
	%b = 1;
	J0 = 2*10^-6; 
	m = 1;
	b = 1/(m*(((1.3806503e-23)*300)/1.60217646e-19));
	for K = 1:1:size_row
		nonlin_p_prfkt(K,1) = J0*(exp(b*(real(sin_in(K,1))))-1);
		nonlin_n_prfkt(K,1) = J0*(exp(b*(-1)*(real(sin_in(K,1))))-1);
	end
	%	nonlin_prfkt = nonlin_p_prfkt + nonlin_n_prfkt;
		nonlin_prfkt = nonlin_p_prfkt;
	
	sin_out_prfkt = filter(Hd,nonlin_prfkt); %���������� ���������� �������
	%���������� ��������� ����� � ����������� ���� ���������� ������� �� ������ �������
	size_row = size(real(sin_out_prfkt),1);
	N = 1;
	L = 1;
	for count = 2:1:(size_row-1)
		count_l = count - 1;
		count_h = count + 1;
		% ���������� ��������� �����
		if sin_out_prfkt(count_l) < sin_out_prfkt(count)
			if sin_out_prfkt(count_h) < sin_out_prfkt(count)
				high_point_sin_out_prfkt(N,1) = sin_out_prfkt(count); 	% ������ ������ ���������� ������� �� ������ �������
				N_high_point_sin_out_prfkt(N,1) = count; 				% ������ ��������� ����� ������ ���������� ������� �� ������ �������
				N = N + 1;
			end
		end
		% ���������� ����� ����������� � ����
        %������ ������ �� ��������� ������� ��������������� �������
        if sin_out_prfkt(count_l) >= 0
			if sin_out_prfkt(count) < 0
				N_zero_point_sin_out_prfkt(L,1) = count_l; % ������ ������� ����� ����������� ���� ���������� ������� �� ������ �������
				K = abs(sin_out_prfkt(count_l)/sin_out_prfkt(count));		
				zero_point_sin_out_prfkt(L,1) = count_l + K/(1+K); %������ ����������� ���������� ������� �� ������ ������� � 0	
%				steepnees_sin_out_prfkt(L,1) = (step*(steepnees_shift*2+1))/(abs(sin_out_prfkt(count + steepnees_shift)) + abs(sin_out_prfkt(count - steepnees_shift))); % T/V [C/B]
                L = L + 1;
            end
        end	
	end 
	
	%sin(2*pi*100e6*step*steepnees_shift)=0.012566039883353
	steepnees_sin_out_prfkt_i = (step*(steepnees_shift*2))/(2*abs(sin(2*pi*100e6*step*steepnees_shift)));% T/V [C/B]

	for M = M_start:M_step:M_stop
		%size_row = size(noise,1);
		size_row = size(sin_in,1);
		%noise1 = randn(1, size_row)*100000;
		
		% R = NORMRND(MU,SIGMA,M,N,) mu - ��� ��������; SIGMA - ���(��������� = SIGMA^2)
		% r = M + sqrt(D)*randn; % M � D - ������ �������� �� � ���������
		
		%noise_k = M*(noise1');

		noise_k = NORMRND(0,sqrt(M),size_row,1);
%		noise_k = NORMRND(0,M,size_row,1);

		ampl_noize(K_Mat,1) = M;
		noise_k_filt = filter(Hd_noise,noise_k);
		
%		for K = 1:1:size_row
%			Disp_ampl_noise(K_Mat,1) = Disp_ampl_noise(K_Mat,1) + (noise_k_filt(K,1))^2;		
%		end		
%		Disp_ampl_noise(K_Mat,1) = Disp_ampl_noise(K_Mat,1)/size_row; %��������� ������������� ����
			
%		sin_noise = real(sin_phaze_noise) + noise_k; %�������������� ������ � ������� � ���������� �����

		sin_noise = real(sin_in) + noise_k; %�������������� ������ ��� �������� ���� � ���������� �����
%		sin_noise = real(sin_in) + noise_k_filt; %�������������� ������ ��� �������� ���� � ���������� �����

		size_row = size(sin_noise,1);
		nonlin_p = zeros(size_row,1);
		nonlin_n = zeros(size_row,1);
		nonlin = zeros(size_row,1);
		for K = 1:1:size_row
			nonlin_p(K,1) = J0*(exp(b*(real(sin_noise(K,1))))-1);
			nonlin_n(K,1) = J0*(exp(b*(-1)*(real(sin_noise(K,1))))-1);
		end
		%nonlin = nonlin_p + nonlin_n;
		nonlin = nonlin_p;
		sin_out = filter(Hd,nonlin); %���������� ������� ����� ����������� �������� ���������� ������� � ���������� ���
		
		%���������� ��� �������� c������ �� ������ �������
		size_row = size(N_high_point_sin_out_prfkt,1);
		start_size_row = 500;
		for K = start_size_row:1:size_row
			high_point_sin_out(K,1) = sin_out(N_high_point_sin_out_prfkt(K,1),1);
			diff_high_point_sin_out(K,1) = high_point_sin_out_prfkt(K,1) - high_point_sin_out(K,1);	
			
			Mat_sin_out_Y_high(K_Mat,1) = Mat_sin_out_Y_high(K_Mat,1) + high_point_sin_out(K,1); %������ �������			
			Mat_sin_out_Y_diff(K_Mat,1) = Mat_sin_out_Y_diff(K_Mat,1) + diff_high_point_sin_out(K,1);

			zero = 0;
			P = 1;
			while zero == 0
				if sin_out((N_high_point_sin_out_prfkt(K,1)+P),1) >= 0
					if sin_out((N_high_point_sin_out_prfkt(K,1)+P+1),1) < 0
						Y = abs(sin_out((N_high_point_sin_out_prfkt(K,1)+P),1)/sin_out((N_high_point_sin_out_prfkt(K,1)+P+1),1));
						zero_point_sin_out(K,1) = (N_high_point_sin_out_prfkt(K,1)+P) + Y/(1+Y);
						zero = 1;					
					end
				end
				P = P + 1;
			end

			diff_zero_point_sin_out(K,1) = (zero_point_sin_out_prfkt(K,1) - zero_point_sin_out(K,1))*step;
			Mat_sin_out_X(K_Mat,1) = Mat_sin_out_X(K_Mat,1) + diff_zero_point_sin_out(K,1);

			diff_N_zero_point_sin_out_prfkt(K,1) = sin_out(N_zero_point_sin_out_prfkt(K,1),1) - sin_out_prfkt(N_zero_point_sin_out_prfkt(K,1),1);
			
			Mat_sin_out_N_Z(K_Mat,1) = Mat_sin_out_N_Z(K_Mat,1) + sin_out(N_zero_point_sin_out_prfkt(K,1),1);
			Mat_sin_out_N_Z_diff(K_Mat,1) = Mat_sin_out_N_Z_diff(K_Mat,1) + diff_N_zero_point_sin_out_prfkt(K,1);						
		end
		
		Mat_sin_out_Y_high(K_Mat,1) = Mat_sin_out_Y_high(K_Mat,1)/(size_row - start_size_row + 1);
		Mat_sin_out_Y_diff(K_Mat,1) = Mat_sin_out_Y_diff(K_Mat,1)/(size_row - start_size_row + 1);
		Mat_sin_out_X(K_Mat,1) = Mat_sin_out_X(K_Mat,1)/(size_row - start_size_row + 1);
		Mat_sin_out_N_Z(K_Mat,1) = Mat_sin_out_N_Z(K_Mat,1)/(size_row - start_size_row + 1);
		Mat_sin_out_N_Z_diff(K_Mat,1) = Mat_sin_out_N_Z_diff(K_Mat,1)/(size_row - start_size_row + 1);

		%���������� ��������� 
		for K = start_size_row:1:size_row
			Disp_sin_out_Y_high(K_Mat,1) = Disp_sin_out_Y_high(K_Mat,1) + (high_point_sin_out(K,1) - Mat_sin_out_Y_high(K_Mat,1))^2;
			Disp_sin_out_Y_diff(K_Mat,1) = Disp_sin_out_Y_diff(K_Mat,1) + (diff_high_point_sin_out(K,1) - Mat_sin_out_Y_diff(K_Mat,1))^2;
	
			Disp_sin_out_X(K_Mat,1) = Disp_sin_out_X(K_Mat,1) + (diff_zero_point_sin_out(K,1) - Mat_sin_out_X(K_Mat,1))^2;
	
			Disp_sin_out_N_Z_high(K_Mat,1) = Disp_sin_out_N_Z_high(K_Mat,1) + (sin_out(N_zero_point_sin_out_prfkt(K,1),1) - Mat_sin_out_N_Z(K_Mat,1))^2;
			Disp_sin_out_N_Z_diff(K_Mat,1) = Disp_sin_out_N_Z_diff(K_Mat,1) + (diff_N_zero_point_sin_out_prfkt(K,1) - Mat_sin_out_N_Z_diff(K_Mat,1))^2;						
		end
		
		% change_disp
		% Disp_sin_out_Y_high(K_Mat,1) = (sqrt(Disp_sin_out_Y_high(K_Mat,1)))/sqrt(size_row - start_size_row + 1);
		% Disp_sin_out_Z(K_Mat,1) = (sqrt(Disp_sin_out_Z(K_Mat,1)))/sqrt(size_row - start_size_row + 1);	

		Disp_sin_out_Y_high(K_Mat,1) = ((Disp_sin_out_Y_high(K_Mat,1)))/(size_row - start_size_row + 1);
		Disp_sin_out_Y_diff(K_Mat,1) = ((Disp_sin_out_Y_diff(K_Mat,1)))/(size_row - start_size_row + 1);
		Disp_sin_out_X(K_Mat,1) = ((Disp_sin_out_X(K_Mat,1)))/(size_row - start_size_row + 1);		
		Disp_sin_out_N_Z_high(K_Mat,1) = ((Disp_sin_out_N_Z_high(K_Mat,1)))/(size_row - start_size_row + 1);		
		Disp_sin_out_N_Z_diff(K_Mat,1) = ((Disp_sin_out_N_Z_diff(K_Mat,1)))/(size_row - start_size_row + 1);		

		K_Mat = K_Mat + 1;
        K_Mat
    end
	
	size_row = K_Mat - 1;
	for K = 1:1:size_row
		Disp_sin_out_Z(K,1) = sqrt(abs((Disp_sin_out_X(K,1))^2 + (Disp_sin_out_Y_high(K,1))^2));
		Disp_sin_out_N_X(K,1) = sqrt(abs((Disp_sin_out_N_Z_high(K,1))^2 - (Disp_sin_out_Y_high(K,1))^2));
		Disp_sin_out_N_X_t(K,1) = Disp_sin_out_N_X(K,1) * steepnees_sin_out_prfkt_i;
	end
	
	% figure
	% plot(sin_out_prfkt);
	% hold on;
	% plot(zero_point_sin_out_prfkt,0,'o');
	% title('sin_out_prfkt(zero)');
	% figure
	% plot(sin_out);
	% hold on;
	% plot(zero_point_sin_out,0,'o');
	% title('sin_out(zero)');
	% figure
	% plot(sin_out_prfkt,'.');
	% hold on;
	% plot(N_high_point_sin_out_prfkt,high_point_sin_out_prfkt,'o');
	% title('sin_out_prfkt(high)');
	% figure
	% plot(sin_out,'.');
	% hold on;
	% plot(N_high_point_sin_out_prfkt,high_point_sin_out,'o');
	% title('sin_out(high)');			
	% figure
	% plot(Mat_sin_out_Y_high,'r');
	% title('��� �������� Y');
	% figure
	% plot(Mat_sin_out_X,'g');
	% title('��� �������� X');
		

	%!!!! ��� ��� � �� ���������. �������� � �������
	% figure
	% plot(Disp_sin_out_Y_high,'r');
	% title('��������� Y');
	% figure
	% plot(Disp_sin_out_X,'g');
	% title('��������� X');
	% figure
	% plot(Disp_sin_out_Z,'b');
	% title('��������� Z');
	
	figure
	plot(ampl_noize,Disp_sin_out_Y_high,'r');
	grid on;
	title('Disp_sin_out_Y_high(r)+Disp_sin_out_Y_diff(g)');
	hold on;
	plot(ampl_noize,Disp_sin_out_Y_diff,'g');
	
	figure
	plot(ampl_noize,Disp_sin_out_X,'g');
	grid on;
	title('��������� X');
	
	figure
	plot(ampl_noize,Disp_sin_out_N_Z_high,'r');
	grid on;
	title('Disp_sin_out_N_Z_high(r)+Disp_sin_out_N_Z_diff(g)');
    hold on;
	plot(ampl_noize,Disp_sin_out_N_Z_diff,'g');
	
	figure
	plot(ampl_noize,Disp_sin_out_N_X,'b');
	grid on;
	title('��������� N_X');
	
	figure
	plot(ampl_noize,Disp_sin_out_N_X_t,'b');
	grid on;
	title('��������� N_X_t');	
	
	figure
	plot(ampl_noize,Disp_sin_out_N_Z_high,'b');
	grid on;
	title('Disp_sin_out_N_Z_high(b)+Disp_sin_out_Y_high(r)+Disp_sin_out_N_X(g)');
	hold on;
	plot(ampl_noize,Disp_sin_out_Y_high,'r');
	hold on;
	plot(ampl_noize,Disp_sin_out_N_X,'.g');
	
