%%
%������
Fs = 1e+011;  % Sampling Frequency
Fstop1 = 75000000;    % First Stopband Frequency
Fpass1 = 80000000;    % First Passband Frequency
Fpass2 = 120000000;   % Second Passband Frequency
Fstop2 = 125000000;   % Second Stopband Frequency
Astop1 = 60;          % First Stopband Attenuation (dB)
Apass  = 1;           % Passband Ripple (dB)
Astop2 = 60;          % Second Stopband Attenuation (dB)
match  = 'stopband';  % Band to match exactly
h  = fdesign.bandpass(Fstop1, Fpass1, Fpass2, Fstop2, Astop1, Apass, ...
                      Astop2, Fs);
Hd = design(h, 'cheby2', 'MatchExactly', match);

U = 1;
%����� ��������� ������������ �������
% 0 ������ ������� c ����������� ��������� ������������ �������� �������
% 1 �����n ������� c ����������� ��������� ������������ ������� ������� 
alg = 3;

%M_start = 00000*10^(-12);
%M_step = 500*10^(-12);
%M_stop = 800000*10^(-12);
%T = 0.00001; %����� �������������
T = 0.0001; %����� �������������

step = 0.01*10^-9; %������� ��������� �������
K_T = T/step;
t1 = 20*10^-9;
t2 = 10*10^-9;
K_t1 = t1/step;
K_t2 = t2/step;

M_start =0*10^(-12);
M_step = 100*10^(-12);
M_stop = 50000*10^(-12);



U_max = ceil((M_start-M_stop)/M_step);
jiter_sin_noise = zeros(U_max,1);
jiter_sin_out = zeros(U_max,1);
noise_raz = zeros(U_max,1);


if alg == 1
	% ������ ������� c ����������� ��������� ������������ �������� ������� ��� ������ ����� � ������������� ������������ ����
	for M = M_start:M_step:M_stop
		noise_k = M*noise;
		sin_noise = real(sin_phaze_noise) + noise_k; %�������������� ������ � ������������� �����

		size_row = size(sin_noise,1);
		nonlin_p = zeros(size_row,1);
		nonlin_n = zeros(size_row,1);
		nonlin = zeros(size_row,1);
		J0 = 100*10^-6;
		b = 1;
		for K = 1:1:size_row
			nonlin_p(K,1) = J0*(exp(b*(real(sin_noise(K,1))))-1);
			nonlin_n(K,1) = J0*(exp(b*(-1)*(real(sin_noise(K,1))))-1);
		end
		nonlin = nonlin_p + nonlin_n;
		sin_out = filter(Hd,nonlin); %����������

		%������ ��������
		size_row = size(sin_noise,1)-1; %����� ���������
		step = 0.01*10^-9; %������� ��������� �������
		time = 0;
		t1 = 20*10^-9;
		t2 = 10*10^-9;
		N = 1;
		P = 0;
		P1 = 0;
%        T = zeros(size_row,1);
		for num_row = 1:size_row
			num_row_2 = num_row + 1;
			if sin_noise(num_row) >= 0
			   if sin_noise(num_row_2) < 0
				   K = abs(sin_noise(num_row)/sin_noise(num_row_2));
				   P = time + step*K/(1+K);
				   if P1 == 0
						if P > t1/10
%						if P > 2*t1/3
						   T(N,1) = P;
						   N = N + 1;
						   P1 = P;
					   end
				   else
					   if (P - P1) >= t1*0.9 || (P - P1) <= t1*0.1
						   T(N,1) = P;
						   N = N + 1;
						   P1 = P;
					   end 
				   end
			   end
			end    
			time = time + step;
		end
		size_row = size(sin_out,1)-1;
		time = 0;
		N = 1;
		P = 0;
		P1 = 0;
%        T2 = zeros(size_row,1);
		for num_row = 1:size_row
			num_row_2 = num_row + 1;
			if sin_out(num_row) >= 0
			   if sin_out(num_row_2) < 0
				   K = abs(sin_out(num_row)/sin_out(num_row_2));
				   P = time + step*K/(1+K);
				   if P1 == 0
						if P > t2/10
%						if P > 2*t1/3
						   T2(N,1) = P;
						   N = N + 1;
						   P1 = P;
					   end
				   else
					   if (P - P1) >= t2*0.9 || (P - P1) <= t2*0.1
						   T2(N,1) = P;
						   N = N + 1;
						   P1 = P;
					   end 
				   end
			   end
			end    
			time = time + step;
		end

		size_row = size(T,1)-1;
		K = 0;
		N = 1;
		interval = 0;
		sqrt = 0;
%        sqr = zeros(size_row,1);
%        Per_sqr = zeros(size_row,1);
		for num_row = 1:size_row
			num_row_2 = num_row + 1;
			res = T(num_row_2,1)- T(num_row,1);
			if res <= (1*10^-9)
				K = K + 1;
				sqr(K,1) = res;
			end   
			if res >= (19*10^-9)
				if K > 0
					for l = 1:K
						for s = 1:l
							sqrt = sqrt + sqr(s,1);
						end
						interval = interval + sqrt;
						sqrt = 0;
					end
					if N > 1
						interval = (Per_sqr(1,(N-1))*(K+1) + interval)/(K+1);
						Per_sqr(1,(N-1)) = interval;
					end
					K = 0;
					sqrt = 0;
					interval = 0;
				end
				Per_sqr(1,N) = res;
				N = N + 1;
			 end
		end

		jiter_sin_noise(U,1) = max(Per_sqr(1,100:(N-1))) - min(Per_sqr(1,100:(N-1)));

		size_row = size(T2,1)-1;
		K = 0;
		N = 1;
		interval = 0;
		sqrt = 0;
%        sqr = zeros(size_row,1);
%        Per_sqr2 = zeros(size_row,1);
		for num_row = 1:size_row
			num_row_2 = num_row + 1;
			res = T2(num_row_2,1)- T2(num_row,1);
			if res <= (1*10^-9)
				K = K + 1;
				sqr(K,1) = res;
			end   
			if res >= (9*10^-9)
				if K > 0
					for l = 1:K
						for s = 1:l
							sqrt = sqrt + sqr(s,1);
						end
						interval = interval + sqrt;
						sqrt = 0;
					end
					if N > 1
						interval = (Per_sqr2(1,(N-1))*(K+1) + interval)/(K+1);
						Per_sqr2(1,(N-1)) = interval;
					end
					K = 0;
					sqrt = 0;
					interval = 0;
				end
				Per_sqr2(1,N) = res;
				N = N + 1;
			end
		end

		jiter_sin_out(U,1) = max(Per_sqr2(1,100:(N-1))) - min(Per_sqr2(1,100:(N-1)));
        noise_raz(U,1) = max(noise_k) - min(noise_k);
		U = U + 1
	end
end

if alg == 0  %0
	% ����� ������� c ����������� ��������� ������������ ������� ������� ��� ������ ����� � ������������� ������������ ����
	for M = M_start:M_step:M_stop
		noise_k = M*noise;
		sin_noise = real(sin_phaze_noise) + noise_k; %�������������� ������ � ������������� �����
		size_row = size(sin_noise,1);
		nonlin_p = zeros(size_row,1);
		nonlin_n = zeros(size_row,1);
		nonlin = zeros(size_row,1);
		J0 = 100*10^-6;
		b = 1;
		for K = 1:1:size_row
			nonlin_p(K,1) = J0*(exp(b*(real(sin_noise(K,1))))-1);
			nonlin_n(K,1) = J0*(exp(b*(-1)*(real(sin_noise(K,1))))-1);
		end
		nonlin = nonlin_p + nonlin_n;
		sin_out = filter(Hd,nonlin); %����������

		%������ ��������
		size_row = size(sin_noise,1)-1; %����� ���������
		step = 0.01*10^-9; %������� ��������� �������
		time = 0;
		t1 = 20*10^-9;
		t2 = 10*10^-9;
		N = 1;
		P = 0;
		P1 = 0;
%        T = zeros(size_row,1);
		for num_row = 1:size_row
			num_row_2 = num_row + 1;
			if sin_noise(num_row) >= 0
				if sin_noise(num_row_2) < 0
					K = abs(sin_noise(num_row)/sin_noise(num_row_2));
					P = time + step*K/(1+K);
					if P1 == 0
						if P > t1/10
%						if P > 2*t1/3
							T(N,1) = P;
							N = N + 1;
							P1 = P;
						end
					else
						if (P - P1) >= t1*0.9 || (P - P1) <= t1*0.1
							T(N,1) = P;
							N = N + 1;
							P1 = P;
						end 
					end
				end
			end    
			time = time + step;
		end
		size_row = size(sin_out,1)-1;
		time = 0;
		N = 1;
		P = 0;
		P1 = 0;
%        T2 = zeros(size_row,1);
		for num_row = 1:size_row
			num_row_2 = num_row + 1;
			if sin_out(num_row) >= 0
			   if sin_out(num_row_2) < 0
				   K = abs(sin_out(num_row)/sin_out(num_row_2));
				   P = time + step*K/(1+K);
				   if P1 == 0
						if P > t2/10
%						if P > 2*t1/3
						   T2(N,1) = P;
						   N = N + 1;
						   P1 = P;
					   end
				   else
					   if (P - P1) >= t2*0.9 || (P - P1) <= t2*0.1
						   T2(N,1) = P;
						   N = N + 1;
						   P1 = P;
					   end 
				   end
			   end
			end    
			time = time + step;
		end

		size_row = size(T,1)-1;
		K = 0;
		N = 1;
		interval = 0;
		sqrt = 0;
%        sqr = zeros(size_row,1);
%        Per_sqr = zeros(size_row,1);
		for num_row = 1:size_row
			num_row_2 = num_row + 1;
			res = T(num_row_2,1)- T(num_row,1);
			if res <= (1*10^-9)
				K = K + 1;
				sqr(K) = res;
			end   
			if res >= (19*10^-9)
				if K > 0 || N > 1
					for l = 1:K
%						for s = 1:l
							Per_sqr(1,N) = Per_sqr(1,(N-1)) + sqr(l);
							N = N + 1;
%						end
					end
					K = 0;
				end
				Per_sqr(1,N) = res;
				N = N + 1;
				K = 0; 
			 end
		end
		jiter_sin_noise(U,1) = max(Per_sqr(1,100:(N-1))) - min(Per_sqr(1,100:(N-1)));

		size_row = size(T2,1)-1;
		K = 0;
		N = 1;
		interval = 0;
		sqrt = 0;
%        sqr = zeros(size_row,1);
%        Per_sqr2 = zeros(size_row,1);
		for num_row = 1:size_row
			num_row_2 = num_row + 1;
			res = T2(num_row_2,1)- T2(num_row,1);
			if res <= (1*10^-9)
				K = K + 1;
				sqr(K) = res;
			end   
			if res >= (9*10^-9)
				if K > 0 || N > 1
					for l = 1:K
%						for s = 1:l
							Per_sqr2(1,N) = Per_sqr2(1,(N-1)) + sqr(l);
							N = N + 1;                    
%						end
					end
					K = 0;
				end
				Per_sqr2(1,N) = res;
				N = N + 1;
			end
		end
		jiter_sin_out(U,1) = max(Per_sqr2(1,100:(N-1))) - min(Per_sqr2(1,100:(N-1)));
		noise_raz(U,1) = max(noise_k) - min(noise_k);
		U = U + 1
	end
end
	
if alg == 2
	% ������ ������� � ����������� ��������� �������� �������
	N_j = 1;
	for M = M_start:M_step:M_stop
		
		size_row = size(noise,1);
		noise1 = randn(1, size_row)*100000;

%		noise_k = M*noise;
		noise_k = M*(noise1');		
		sin_noise = real(sin_phaze_noise) + noise_k; %�������������� ������ � ������������� �����
		noise_k = zeros;

		size_row = size(sin_noise,1);
		nonlin_p = zeros(size_row,1);
		nonlin_n = zeros(size_row,1);
		nonlin = zeros(size_row,1);
		J0 = 100*10^-6;
		b = 1;
		for K = 1:1:size_row
			nonlin_p(K,1) = J0*(exp(b*(real(sin_noise(K,1))))-1);
			nonlin_n(K,1) = J0*(exp(b*(-1)*(real(sin_noise(K,1))))-1);
		end
		nonlin = nonlin_p + nonlin_n;
		sin_out = filter(Hd,nonlin); %����������

		%������ ��������
		size_row = size(sin_noise,1)-1; %����� ���������
		step = 0.01*10^-9; %������� ��������� �������
		time = 0;
		t1 = 20*10^-9;
		t2 = 10*10^-9;
		N = 1;
		P = 0;
		P1 = 0;
		
%        T = zeros(size_row,1);
		
		for num_row = 1:size_row
			num_row_2 = num_row + 1;
			if sin_noise(num_row) >= 0
			   if sin_noise(num_row_2) < 0
				   K = abs(sin_noise(num_row)/sin_noise(num_row_2));
				   P = time + step*K/(1+K);
				   if P1 == 0
%						if P > t1/10
						if P > 2*t1/3
						   T(N,1) = P;
						   N = N + 1;
						   P1 = P;
					   end
				   else
					   if (P - P1) >= t1*0.9 | (P - P1) <= t1*0.1
						   T(N,1) = P;
						   N = N + 1;
						   P1 = P;
					   end 
				   end
			   end
			end    
			time = time + step;
		end
		size_row = size(sin_out,1)-1;
		time = 0;
		N = 1;
		P = 0;
		P1 = 0;
		
%        T2 = zeros(size_row,1);
		
		for num_row = 1:size_row
			num_row_2 = num_row + 1;
			if sin_out(num_row) >= 0
			   if sin_out(num_row_2) < 0
				   K = abs(sin_out(num_row)/sin_out(num_row_2));
				   P = time + step*K/(1+K);
				   if P1 == 0
%						if P > t2/10
						if P > 2*t1/3
						   T2(N,1) = P;
						   N = N + 1;
						   P1 = P;
					   end
				   else
					   if (P - P1) >= t2*0.9 | (P - P1) <= t2*0.1
						   T2(N,1) = P;
						   N = N + 1;
						   P1 = P;
					   end 
				   end
			   end
			end    
			time = time + step;
		end
		
		
		size_row = size(T,1) - 1;
		jitt_sin_noise(1) = T(1);
		L1 = 0;
		N1 = 2;
		diff = 0;
		for num_row = 1:size_row
			num_row_2 = num_row + 1;
			diff = T(num_row_2) - T(num_row);
			if diff > t1/2
				L1 = L1 + 1;
				jitt_sin_noise(N1) = T(num_row_2) - t1*L1;
				N1 = N1 + 1;
			else
				jitt_sin_noise(N1) = T(num_row_2) - t1*L1;
				N1 = N1 + 1;
			end
		end
		
		j_sin_noise(N_j) = max(jitt_sin_noise(100:(N1-1))) - min(jitt_sin_noise(100:(N1-1)));
		
		size_row = size(T2,1) - 1;
		jitt_sin_out(1) = T2(1);
		L2 = 0;
		N2 = 2;
		diff = 0;
		for num_row = 1:size_row
			num_row_2 = num_row + 1;
			diff = T2(num_row_2) - T2(num_row);
			if diff > t2/2
				L2 = L2 + 1;
				jitt_sin_out(N2) = T2(num_row_2) - t2*L2;
				N2 = N2 + 1;
			else
				jitt_sin_out(N2) = T2(num_row_2) - t2*L2;
				N2 = N2 + 1;
			end
		end		
		j_sin_out(N_j) = max(jitt_sin_out(100:(N2-1))) - min(jitt_sin_out(100:(N2-1)));
		M_X(N_j) = M;
%		M_X(N_j) = max(noise_k);
		N_j = N_j + 1
		
		T = zeros;
%		jitt_sin_noise = zeros;
		T2 = zeros;
%		jitt_sin_out = zeros;
%		hold on;		
%		plot(sin_noise,'r');	
		
	end

		figure
		plot(M_X,j_sin_noise,'r');
		title('������� ������� � ����� �� ����������');	
		figure
		plot(M_X,j_sin_out,'g');
		title('������� ������� � ����� ����� ����������');


		% figure
		% plot(jitt_sin_noise,'r');
		% title('������� ������� � ����� �� ����������');	
		% figure
		% plot(jitt_sin_out,'g');
		% title('������� ������� � ����� ����� ����������');


		% figure
		% plot(sin_noise,'r');
		% title('������ � ����� �� ����������');
		% figure
		% plot(T,'.r');
		% title('����������� � ���� ������� � ����� �� ����������');
		
		% figure
		% plot(sin_out,'b');
		% title('������ � ����� ����� ����������');		
		% figure
		% plot(T2,'.b');
		% title('����������� � ���� ������� � ����� ����� ����������');	
	
end

if alg ==3


K_Mat = 1;
N_j = 1;
size_row = round((M_stop - M_start)/M_step) + 1; %round - ���������� �� ���������� ������
ampl_noize = zeros(size_row,1);
Mat_sin_out_Y = zeros(size_row,1);
Mat_sin_out_X = zeros(size_row,1);
Disp_sin_out_Y = zeros(size_row,1);
Disp_sin_out_Z = zeros(size_row,1);
Disp_sin_out_X = zeros(size_row,1);

	% ���������� ���������� ���������� �������
	size_row = size(real(sin),1);
	nonlin_p = zeros(size_row,1);
	nonlin_n = zeros(size_row,1);
	nonlin = zeros(size_row,1);
	J0 = 100*10^-6;
	b = 1;
	for K = 1:1:size_row
		nonlin_p(K,1) = J0*(exp(b*(real(sin(K,1))))-1);
		nonlin_n(K,1) = J0*(exp(b*(-1)*(real(sin(K,1))))-1);
	end
	nonlin = nonlin_p + nonlin_n;
	sin_out_prfkt = filter(Hd,nonlin); %���������� ���������� �������

	%���������� ��������� ����� ��������� �������
	size_row = size(real(sin_out_prfkt),1);
	N = 1;
	L = 1;
	for count = 2:1:(size_row-1)
		count_l = count - 1;
		count_h = count + 1;
		if sin_out_prfkt(count_l) < sin_out_prfkt(count)
			if sin_out_prfkt(count_h) < sin_out_prfkt(count)
				high_point_sin_out_prfkt(N,1) = sin_out_prfkt(count); % ������ ������ ���������� ������� �� ������ �������
				N_high_point_sin_out_prfkt(N,1) = count; % ������ ��������� ����� ������ ���������� ������� �� ������ �������
				N = N + 1;
			end
		end
		if sin_out_prfkt(count_l) >= 0
			if sin_out_prfkt(count) < 0
				K = abs(sin_out_prfkt(count_l)/sin_out_prfkt(count));
				zero_point_sin_out_prfkt(L,1) = count_l + K/(1+K); %������ ����������� ���������� ������� �� ������ ������� � 0
				L = L + 1;
			end
		end	
    end
    
    size_row_noise = size(noise,1);
	size_row_N_high_point_sin_out_prfkt = size(N_high_point_sin_out_prfkt,1);
	nonlin_p = zeros(size_row_noise,1);
	nonlin_n = zeros(size_row_noise,1);
	nonlin = zeros(size_row_noise,1);   
	high_point_sin_out = zeros(size_row_N_high_point_sin_out_prfkt,1);  
	diff_high_point_sin_out	= zeros(size_row_N_high_point_sin_out_prfkt,1); 
	zero_point_sin_out = zeros(size_row_N_high_point_sin_out_prfkt,1); 
	diff_zero_point_sin_out = zeros(size_row_N_high_point_sin_out_prfkt,1); 
    
    for M = M_start:M_step:M_stop
		noise1 = randn(1, size_row_noise)*100000;
		noise_k = M*(noise1');
		ampl_noize(K_Mat,1) = max(noise_k);
		sin_noise = real(sin_phaze_noise) + noise_k; %�������������� ������ � ������� � ���������� �����
		J0 = 100*10^-6;
		b = 1;
		for K = 1:1:size_row_noise
			nonlin_p(K,1) = J0*(exp(b*(real(sin_noise(K,1))))-1);
			nonlin_n(K,1) = J0*(exp(b*(-1)*(real(sin_noise(K,1))))-1);
		end
		nonlin = nonlin_p + nonlin_n;
		sin_out = filter(Hd,nonlin); %���������� ������� ����� ����������� �������� ���������� ������� � ���������� ���
		
		%���������� ��� �������� c������ �� ������ �������
		start_size_row = 500;
		for K = start_size_row:1:size_row_N_high_point_sin_out_prfkt
			high_point_sin_out(K,1) = sin_out(N_high_point_sin_out_prfkt(K,1));
			diff_high_point_sin_out(K,1) = high_point_sin_out_prfkt(K,1) - high_point_sin_out(K,1);
%			Mat_sin_out_Y(K_Mat,1) = Mat_sin_out_Y(K_Mat,1) + high_point_sin_out(K,1);
			Mat_sin_out_Y(K_Mat,1) = Mat_sin_out_Y(K_Mat,1) + diff_high_point_sin_out(K,1);
			zero = 0;
			P = 1;
			while zero == 0
				if sin_out((N_high_point_sin_out_prfkt(K,1)+P)) >= 0
					if sin_out((N_high_point_sin_out_prfkt(K,1)+P+1)) < 0
						Y = abs(sin_out((N_high_point_sin_out_prfkt(K,1)+P))/sin_out((N_high_point_sin_out_prfkt(K,1)+P+1)));
						zero_point_sin_out(K,1) = (N_high_point_sin_out_prfkt(K,1)+P) + Y/(1+Y);
						zero = 1;					
					end
				end
				P = P + 1;
			end
			diff_zero_point_sin_out(K,1) = zero_point_sin_out_prfkt(K,1) - zero_point_sin_out(K,1);
			Mat_sin_out_X(K_Mat,1) = Mat_sin_out_X(K_Mat,1) + diff_zero_point_sin_out(K,1);
		end
		Mat_sin_out_Y(K_Mat,1) = Mat_sin_out_Y(K_Mat,1)/(size_row_N_high_point_sin_out_prfkt - start_size_row + 1);
		Mat_sin_out_X(K_Mat,1) = Mat_sin_out_X(K_Mat,1)/(size_row_N_high_point_sin_out_prfkt - start_size_row + 1);

		%���������� ��������� 
		for K = start_size_row:1:size_row_N_high_point_sin_out_prfkt
%			Disp_sin_out_Y(K_Mat,1) = Disp_sin_out_Y(K_Mat,1) + (high_point_sin_out(K,1) - Mat_sin_out_Y(K_Mat,1))^2;
			Disp_sin_out_Y(K_Mat,1) = Disp_sin_out_Y(K_Mat,1) + (diff_high_point_sin_out(K,1) - Mat_sin_out_Y(K_Mat,1))^2;
			Disp_sin_out_Z(K_Mat,1) = Disp_sin_out_Z(K_Mat,1) + (diff_zero_point_sin_out(K,1) - Mat_sin_out_X(K_Mat,1))^2;
		end
		Disp_sin_out_Y(K_Mat,1) = (sqrt(Disp_sin_out_Y(K_Mat,1)))/sqrt(size_row_N_high_point_sin_out_prfkt - start_size_row + 1);
		Disp_sin_out_Z(K_Mat,1) = (sqrt(Disp_sin_out_Z(K_Mat,1)))/sqrt(size_row_N_high_point_sin_out_prfkt - start_size_row + 1);	
		
		
		
		% for L1 = 1:1:((K_T/K_t1)-2)
			% sin_L = round(K_t1/4) + round(K_t1*(L1-1));
			% high_point_sin(L1,1) = real(sin(sin_L));	
			% high_point_sin_phaze_noise(L1,1) = real(sin_phaze_noise(sin_L));
			% high_point_sin_noise(L1,1) = real(sin_noise(sin_L));
			% Mat_sin_phaze_noise_Y(K_Mat,1) = Mat_sin_phaze_noise_Y(K_Mat,1) + high_point_sin_phaze_noise(L1,1);
			% Mat_sin_noise_Y(K_Mat,1) = Mat_sin_noise_Y(K_Mat,1) + high_point_sin_noise(L1,1);
		
%			���������� ���������� � ����
			% sin_L_zero = sin_L + (((1/4)*K_t1)-((1/10)*K_t1));
			% k = 0;
			% while k == 0
				% if real(sin_phaze_noise(sin_L_zero)) >= 0
					% if real(sin_phaze_noise((sin_L_zero+1))) < 0 
						% K = abs(real(sin_phaze_noise((sin_L_zero)))/real(sin_phaze_noise((sin_L_zero+1))));
						% zero_point_sin_phaze_noise(L1,1) = sin_L_zero*step + step*K/(1+K);
						% k = 1;
					% end 
				% end 
				% sin_L_zero = sin_L_zero + 1;
			% end  
			% sin_L_zero = sin_L + (((1/4)*K_t1)-((1/10)*K_t1));
			% k = 0;
			% while k == 0
				% if real(sin_noise(sin_L_zero)) >= 0
					% if real(sin_noise((sin_L_zero+1))) < 0 
						% K = abs(real(sin_noise((sin_L_zero)))/real(sin_noise((sin_L_zero+1))));
						% zero_point_sin_noise(L1,1) = sin_L_zero*step + step*K/(1+K);
						% k = 1;
					% end 
				% end 
				% sin_L_zero = sin_L_zero + 1;
			% end
			% Mat_sin_phaze_noise_X(K_Mat,1) = Mat_sin_phaze_noise_X(K_Mat,1) + zero_point_sin_phaze_noise(L1,1);
			% Mat_sin_noise_X(K_Mat,1) = Mat_sin_noise_X(K_Mat,1) + zero_point_sin_noise(L1,1);

			
			
		% end
%		���������� ���������� ���� ������� ����� �������
		% f = 1e5;
		% k = 0;
		% while (k==0)
			% if sin_out(f) > sin_out(f+1) 
				% if sin_out(f) > sin_out(f-1)
					% k = 1;
				% else
					% f = f + 1;
				% end
			% else
				% f = f + 1;
			% end
		% end
		% L2_start = ceil (f/K_t2); %���������� �� ���������� ������
		% for L2 = L2_start:1:(((K_T-f)/K_t2)-2)
			% sin_L = f + round(K_t2*(L2-1));
			% high_point_sin_out((L2-L2_start+1),1) = real(sin_out(sin_L));
			% Mat_sin_out_Y(K_Mat,1) = Mat_sin_out_Y(K_Mat,1) + high_point_sin_out((L2-L2_start+1),1);
		% end		
		% Mat_sin_phaze_noise_Y(K_Mat,1) = Mat_sin_phaze_noise_Y(K_Mat,1)/L1;
		% Mat_sin_noise_Y(K_Mat,1) = Mat_sin_noise_Y(K_Mat,1)/L1;
		% Mat_sin_out_Y(K_Mat,1) = Mat_sin_out_Y(K_Mat,1)/(L2-L2_start+1);
		
%		���������� ���������
		% for L1 = 1:1:(K_T/K_t1)
			% sin_L = round(K_t1/4) + round(K_t1*(L1-1));
			% high_point_sin(L1,1) = real(sin(sin_L));	
			% high_point_sin_phaze_noise(L1,1) = real(sin_phaze_noise(sin_L));
			% high_point_sin_noise(L1,1) = real(sin_noise(sin_L));
			% Disp_sin_phaze_noise_Y(K_Mat,1) = Disp_sin_phaze_noise_Y(K_Mat,1) + (high_point_sin_phaze_noise(L1,1) - Mat_sin_phaze_noise_Y(K_Mat,1))^2;
			% Disp_sin_noise_Y(K_Mat,1) = Disp_sin_noise_Y(K_Mat,1) + (high_point_sin_noise(L1,1) - Mat_sin_noise_Y(K_Mat,1))^2;
		% end
		% for L2 = L2_start:1:(((K_T-f)/K_t2)-2)
			% sin_L = f + round(K_t2*(L2-1));
			% high_point_sin_out((L2-L2_start+1),1) = real(sin_out(sin_L));
			% Disp_sin_out_Y(K_Mat,1) = Disp_sin_out_Y(K_Mat,1) + (high_point_sin_out((L2-L2_start+1),1) - Mat_sin_out_Y(K_Mat,1))^2;
		% end			
		% Disp_sin_noise_Y(K_Mat,1) = sqrt(Disp_sin_noise_Y(K_Mat,1))/sqrt(L1);
		% Disp_sin_phaze_noise_Y(K_Mat,1) = sqrt(Disp_sin_phaze_noise_Y(K_Mat,1))/sqrt(L1);
		% Disp_sin_out_Y(K_Mat,1) = sqrt(Disp_sin_out_Y(K_Mat,1))/sqrt(L2-L2_start+1);
		
		K_Mat = K_Mat + 1;
		K_Mat
	end
	
	size_row = K_Mat - 1;
	for K = 1:1:size_row
		Disp_sin_out_X(K,1) = sqrt((Disp_sin_out_Z(K,1))^2 - (Disp_sin_out_Y(K,1))^2);
	end
	
			% figure
			% plot(sin_out_prfkt);
			% hold on;
			% plot(zero_point_sin_out_prfkt,0,'o');
			% title('sin_out_prfkt(zero)');
			% figure
			% plot(sin_out);
			% hold on;
			% plot(zero_point_sin_out,0,'o');
			% title('sin_out(zero)');
			% figure
			% plot(sin_out_prfkt,'.');
			% hold on;
			% plot(N_high_point_sin_out_prfkt,high_point_sin_out_prfkt,'o');
			% title('sin_out_prfkt(high)');
			% figure
			% plot(sin_out,'.');
			% hold on;
			% plot(N_high_point_sin_out_prfkt,high_point_sin_out,'o');
			% title('sin_out(high)');			

figure
plot(Mat_sin_out_Y,'r');
title('��� �������� Y');
figure
plot(Mat_sin_out_X,'g');
title('��� �������� X');

figure
plot(Disp_sin_out_Y,'r');
title('��������� Y');
figure
plot(Disp_sin_out_X,'g');
title('��������� X');
figure
plot(Disp_sin_out_Z,'b');
title('��������� Z');


end




%figure
%plot(noise_raz,jiter_sin_noise,'r');
%figure
%plot(noise_raz,jiter_sin_out,'g');

%%
%���������� ������������
Fd = 1/(0.01*10^-9); %������� �������������
%FftL=1048576;% ���������� ����� ����� �������
FftL = size(sin,1);
FftS1=abs(fft(real(sin),FftL));% ��������� �������������� ����� �������
FftS1=2*FftS1./FftL;% ���������� ������� �� ���������
FftS1(1)=FftS1(1)/2;% ���������� ���������� ������������ � �������
F=0:Fd/FftL:Fd/2-Fd/FftL;% ������ ������ ������������ ������� �����
figure% ������� ����� ����
plot(F,20*log10(FftS1(1:length(F))),'r');% ���������� ������� ����� �������
title('������');% ������� �������

FftL = size(sin_phaze_noise,1);
FftS2=abs(fft(real(sin_phaze_noise),FftL));% ��������� �������������� ����� �������
FftS2=2*FftS2./FftL;% ���������� ������� �� ���������
FftS2(1)=FftS2(1)/2;% ���������� ���������� ������������ � �������
F=0:Fd/FftL:Fd/2-Fd/FftL;% ������ ������ ������������ ������� �����
%figure% ������� ����� ����
hold on;
plot(F,20*log10(FftS2(1:length(F))),'g');% ���������� ������� ����� �������
title('������');% ������� �������

FftL = size(sin_noise,1);
FftS3=abs(fft(real(sin_noise),FftL));% ��������� �������������� ����� �������
FftS3=2*FftS3./FftL;% ���������� ������� �� ���������
FftS3(1)=FftS3(1)/2;% ���������� ���������� ������������ � �������
F=0:Fd/FftL:Fd/2-Fd/FftL;% ������ ������ ������������ ������� �����
%figure% ������� ����� ����
hold on;
plot(F,20*log10(FftS3(1:length(F))),'b');% ���������� ������� ����� �������
title('������');% ������� �������

FftL = size(sin_out,1);
FftS4=abs(fft(real(sin_out),FftL));% ��������� �������������� ����� �������
FftS4=2*FftS4./FftL;% ���������� ������� �� ���������
FftS4(1)=FftS4(1)/2;% ���������� ���������� ������������ � �������
F=0:Fd/FftL:Fd/2-Fd/FftL;% ������ ������ ������������ ������� �����
%figure% ������� ����� ����
hold on;
plot(F,20*log10(FftS4(1:length(F))),'c');% ���������� ������� ����� �������
title('������');% ������� �������