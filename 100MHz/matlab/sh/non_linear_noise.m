%% ������ � ���������� �������
Fd = 1/(0.01*10^-9); %������� �������������
%FftL=1048576;% ���������� ����� ����� �������
FftL = size(sin,1);
FftS1=abs(fft(real(sin),FftL));% ��������� �������������� ����� �������
FftS1=2*FftS1./FftL;% ���������� ������� �� ���������
FftS1(1)=FftS1(1)/2;% ���������� ���������� ������������ � �������
F=0:Fd/FftL:Fd/2-Fd/FftL;% ������ ������ ������������ ������� �����
figure% ������� ����� ����
plot(F,20*log10(FftS1(1:length(F))),'r');% ���������� ������� ����� �������
title('������');% ������� �������

FftL = size(sin_phaze_noise,1);
FftS2=abs(fft(real(sin_phaze_noise),FftL));% ��������� �������������� ����� �������
FftS2=2*FftS2./FftL;% ���������� ������� �� ���������
FftS2(1)=FftS2(1)/2;% ���������� ���������� ������������ � �������
F=0:Fd/FftL:Fd/2-Fd/FftL;% ������ ������ ������������ ������� �����
%figure% ������� ����� ����
hold on;
plot(F,20*log10(FftS2(1:length(F))),'g');% ���������� ������� ����� �������
title('������');% ������� �������

FftL = size(sin_noise,1);
FftS3=abs(fft(real(sin_noise),FftL));% ��������� �������������� ����� �������
FftS3=2*FftS3./FftL;% ���������� ������� �� ���������
FftS3(1)=FftS3(1)/2;% ���������� ���������� ������������ � �������
F=0:Fd/FftL:Fd/2-Fd/FftL;% ������ ������ ������������ ������� �����
%figure% ������� ����� ����
hold on;
plot(F,20*log10(FftS3(1:length(F))),'b');% ���������� ������� ����� �������
title('������');% ������� �������

FftL = size(sin_out,1);
FftS3=abs(fft(real(sin_out),FftL));% ��������� �������������� ����� �������
FftS3=2*FftS3./FftL;% ���������� ������� �� ���������
FftS3(1)=FftS3(1)/2;% ���������� ���������� ������������ � �������
F=0:Fd/FftL:Fd/2-Fd/FftL;% ������ ������ ������������ ������� �����
%figure% ������� ����� ����
hold on;
plot(F,20*log10(FftS3(1:length(F))),'c');% ���������� ������� ����� �������
title('������');% ������� �������

%% ������ ������� c ����������� ��������� ������������ �������� ������� ��� ������ ����� � ������������� ������������ ����
size_row = size(sin_noise,1)-1; %����� ���������
step = 0.01*10^-9; %������� ��������� �������
time = 0;
t1 = 20*10^-9;
t2 = 10*10^-9;
N = 1;
P1 = 0;
for num_row = 1:size_row
    num_row_2 = num_row + 1;
    size_row - num_row
    if sin_noise(num_row) >= 0
       if sin_noise(num_row_2) < 0
           K = abs(sin_noise(num_row)/sin_noise(num_row_2));
           P = time + step*K/(1+K);
           if P1 == 0
               if P > t1/10
                   T(N,1) = P;
                   N = N + 1;
                   P1 = P;
               end
           else
               if (P - P1) >= t1*0.9 || (P - P1) <= t1*0.1
                   T(N,1) = P;
                   N = N + 1;
                   P1 = P;
               end 
           end
       end
	end    
	time = time + step;
end
size_row = size(sin_out,1)-1;
time = 0;
N = 1;
P1 = 0;
for num_row = 1:size_row
    num_row_2 = num_row + 1;
    size_row - num_row
    if sin_out(num_row) >= 0
       if sin_out(num_row_2) < 0
           K = abs(sin_out(num_row)/sin_out(num_row_2));
           P = time + step*K/(1+K);
           if P1 == 0
               if P > t2/10
                   T2(N,1) = P;
                   N = N + 1;
                   P1 = P;
               end
           else
               if (P - P1) >= t2*0.9 || (P - P1) <= t2*0.1
                   T2(N,1) = P;
                   N = N + 1;
                   P1 = P;
               end 
           end
       end
	end    
	time = time + step;
end

size_row = size(T,1)-1;
K = 0;
N = 1;
interval = 0;
sqrt = 0;
for num_row = 1:size_row
    num_row_2 = num_row + 1;
    res = T(num_row_2,1)- T(num_row,1);
    if res <= (1*10^-9)
        K = K + 1;
        sqr(K) = res;
    end   
    if res >= (19*10^-9)
        if K > 0
            for l = 1:K
                for s = 1:l
                    sqrt = sqrt + sqr(s);
                end
                interval = interval + sqrt;
                sqrt = 0;
            end
            if N > 1
				interval = (Per_sqr(1,(N-1))*(K+1) + interval)/(K+1);
				Per_sqr(1,(N-1)) = interval;
            end
			K = 0;
            sqrt = 0;
            interval = 0;
        end
        Per_sqr(1,N) = res;
        N = N + 1;
     end
end

figure;
plot(Per_sqr);
jiter_sin_noise = max(Per_sqr(1,100:(N-1))) - min(Per_sqr(1,100:(N-1)))

size_row = size(T2,1)-1;
K = 0;
N = 1;
interval = 0;
sqrt = 0;
for num_row = 1:size_row
    num_row_2 = num_row + 1;
    res = T2(num_row_2,1)- T2(num_row,1);
    if res <= (1*10^-9)
        K = K + 1;
        sqr(K) = res;
    end   
    if res >= (9*10^-9)
        if K > 0
            for l = 1:K
                for s = 1:l
                    sqrt = sqrt + sqr(s);
                end
                interval = interval + sqrt;
                sqrt = 0;
            end
            if N > 1
				interval = (Per_sqr2(1,(N-1))*(K+1) + interval)/(K+1);
				Per_sqr2(1,(N-1)) = interval;
			end
            K = 0;
            sqrt = 0;
            interval = 0;
        end
        Per_sqr2(1,N) = res;
        N = N + 1;
    end
end
figure;
plot(Per_sqr2);

jiter_sin_out = max(Per_sqr2(1,100:(N-1))) - min(Per_sqr2(1,100:(N-1)))

%% ����� ������� c ����������� ��������� ������������ ������� ������� ��� ������ ����� � ������������� ������������ ����
size_row = size(sin_noise,1)-1; %����� ���������
step = 0.01*10^-9; %������� ��������� �������
time = 0;
t1 = 20*10^-9;
t2 = 10*10^-9;
N = 1;
P1 = 0;
for num_row = 1:size_row
    num_row_2 = num_row + 1;
    size_row - num_row
    if sin_noise(num_row) >= 0
       if sin_noise(num_row_2) < 0
           K = abs(sin_noise(num_row)/sin_noise(num_row_2));
           P = time + step*K/(1+K);
           if P1 == 0
               if P > t1/10
                   T(N,1) = P;
                   N = N + 1;
                   P1 = P;
               end
           else
               if (P - P1) >= t1*0.9 || (P - P1) <= t1*0.1
                   T(N,1) = P;
                   N = N + 1;
                   P1 = P;
               end 
           end
       end
	end    
	time = time + step;
end
size_row = size(sin_out,1)-1;
time = 0;
N = 1;
P1 = 0;
for num_row = 1:size_row
    num_row_2 = num_row + 1;
    size_row - num_row
    if sin_out(num_row) >= 0
       if sin_out(num_row_2) < 0
           K = abs(sin_out(num_row)/sin_out(num_row_2));
           P = time + step*K/(1+K);
           if P1 == 0
               if P > t2/10
                   T2(N,1) = P;
                   N = N + 1;
                   P1 = P;
               end
           else
               if (P - P1) >= t2*0.9 || (P - P1) <= t2*0.1
                   T2(N,1) = P;
                   N = N + 1;
                   P1 = P;
               end 
           end
       end
	end    
	time = time + step;
end

size_row = size(T,1)-1;
K = 0;
N = 1;
interval = 0;
sqrt = 0;
for num_row = 1:size_row
    num_row_2 = num_row + 1;
    res = T(num_row_2,1)- T(num_row,1);
    if res <= (1*10^-9)
        K = K + 1;
        sqr(K) = res;
    end   
    if res >= (19*10^-9)
        if K > 0 || N > 1
            for l = 1:K
                for s = 1:l
                    Per_sqr(1,N) = Per_sqr(1,(N-1)) + sqr(K);
					N = N + 1;
                end
            end
			K = 0;
        end
        Per_sqr(1,N) = res;
        N = N + 1;
     end
end
figure;
plot(Per_sqr);
jiter_sin_noise = max(Per_sqr(1,100:(N-1))) - min(Per_sqr(1,100:(N-1)))

size_row = size(T2,1)-1;
K = 0;
N = 1;
interval = 0;
sqrt = 0;
for num_row = 1:size_row
    num_row_2 = num_row + 1;
    res = T2(num_row_2,1)- T2(num_row,1);
    if res <= (1*10^-9)
        K = K + 1;
        sqr(K) = res;
    end   
    if res >= (9*10^-9)
        if K > 0 || N > 1
            for l = 1:K
                for s = 1:l
                    Per_sqr2(1,N) = Per_sqr2(1,(N-1)) + sqr(K);
					N = N + 1;                    
                end
            end
            K = 0;
        end
        Per_sqr2(1,N) = res;
        N = N + 1;
    end
end
figure;
plot(Per_sqr2);

jiter_sin_out = max(Per_sqr2(1,100:(N-1))) - min(Per_sqr2(1,100:(N-1)))


%% ������������� ����������� ������� ����� ���������� ������� � ������
size_row = size(sin_noise,1);
nonlin_p = zeros(size_row,1);
nonlin_n = zeros(size_row,1);
nonlin = zeros(size_row,1);
R = 10;
J0 = 100*10^-6;
b = 1;
for K = 1:1:size_row
	nonlin_p(K,1) = J0*(exp(b*(R*real(sin_noise(K,1))))-1);
	nonlin_n(K,1) = J0*(exp(b*(-1)*(R*real(sin_noise(K,1))))-1);
end
nonlin = nonlin_p + nonlin_n;

Fs = 1e+011;  % Sampling Frequency
Fstop1 = 75000000;    % First Stopband Frequency
Fpass1 = 80000000;    % First Passband Frequency
Fpass2 = 120000000;   % Second Passband Frequency
Fstop2 = 125000000;   % Second Stopband Frequency
Astop1 = 60;          % First Stopband Attenuation (dB)
Apass  = 1;           % Passband Ripple (dB)
Astop2 = 60;          % Second Stopband Attenuation (dB)
match  = 'stopband';  % Band to match exactly
h  = fdesign.bandpass(Fstop1, Fpass1, Fpass2, Fstop2, Astop1, Apass, ...
                      Astop2, Fs);
Hd = design(h, 'cheby2', 'MatchExactly', match);
sin_out = filter(Hd,nonlin);
