%% ������ � ���������� �������
Fd = 1/(0.02*10^-9); %������� �������������
FftL=1048576;% ���������� ����� ����� �������
%FftL = size(sin,1);
%FftL=(number_to - number_from);% ���������� ����� ����� �������
FftS1=abs(fft(real(sin),FftL));% ��������� �������������� ����� �������
FftS1=2*FftS1./FftL;% ���������� ������� �� ���������
FftS1(1)=FftS1(1)/2;% ���������� ���������� ������������ � �������
F=0:Fd/FftL:Fd/2-Fd/FftL;% ������ ������ ������������ ������� �����
figure% ������� ����� ����
plot(F,20*log10(FftS1(1:length(F))),'.r');% ���������� ������� ����� �������
title('������');% ������� �������

%FftL = size(sin,1);
FftS2=abs(fft(real(sin_phaze_noise),FftL));% ��������� �������������� ����� �������
FftS2=2*FftS2./FftL;% ���������� ������� �� ���������
FftS2(1)=FftS2(1)/2;% ���������� ���������� ������������ � �������
F=0:Fd/FftL:Fd/2-Fd/FftL;% ������ ������ ������������ ������� �����
%figure% ������� ����� ����
hold on;
plot(F,20*log10(FftS2(1:length(F))),'.g');% ���������� ������� ����� �������
title('������');% ������� �������

%FftL = size(sin,1);
FftS3=abs(fft(real(sin_noise),FftL));% ��������� �������������� ����� �������
FftS3=2*FftS3./FftL;% ���������� ������� �� ���������
FftS3(1)=FftS3(1)/2;% ���������� ���������� ������������ � �������
F=0:Fd/FftL:Fd/2-Fd/FftL;% ������ ������ ������������ ������� �����
%figure% ������� ����� ����
hold on;
plot(F,20*log10(FftS3(1:length(F))),'.b');% ���������� ������� ����� �������
title('������');% ������� �������


%% ����� � ���������� �������� �������
size_row = size(sin_noise,1)-1; %����� ���������
step = 0.02*10^-9; %������� ��������� �������
time = 0;
N = 1;
P1 = 0;
for num_row = 1:size_row
    num_row_2 = num_row + 1;
    size_row - num_row
    if sin_noise(num_row) >= 0
       if sin_noise(num_row_2) < 0
           K = abs(sin_noise(num_row)/sin_noise(num_row_2));
           P = time + step*K/(1+K);
           if P1 == 0
               T(N,1) = P;
               N = N + 1;
               P1 = P;
           else
               if (P - P1) > 19*10^-9 || (P - P1) < 1*10^-9
                   T(N,1) = P;
                   N = N + 1;
                   P1 = P;
               end 
           end
       end
	end    
	time = time + step;
end
size_row = size(sin_out,1)-1; %����� ���������
time = 0;
N = 1;
P1 = 0;
for num_row = 1:size_row
    num_row_2 = num_row + 1;
    size_row - num_row
    if sin_out(num_row) >= 0
       if sin_out(num_row_2) < 0
           K = abs(sin_out(num_row)/out(num_row_2));
           P = time + step*K/(1+K);
           if P1 == 0
               T2(N,1) = P;
               N = N + 1;
               P1 = P;
           else
               if (P - P1) > 19*10^-9 || (P - P1) < 1*10^-9
                   T2(N,1) = P;
                   N = N + 1;
                   P1 = P;
               end 
           end
       end
	end    
	time = time + step;
end


size_row = size(T,1)-1;
K = 0;
N = 1;
interval = 0;
sqrt = 0;
for num_row = 1:size_row
    num_row_2 = num_row + 1;
    res = T(num_row_2,1)- T(num_row,1);
    if res <= (1*10^-9)
        K = K + 1;
        sqr(K) = res;
    end   
    if res >= (19*10^-9)
        if K > 0
            for l = 1:K
                for s = 1:l
                    sqrt = sqrt + sqr(s);
                end
                interval = interval + sqrt;
                sqrt = 0;
            end
            interval = (Per_sqr(1,(N-1))*(K+1) + interval)/(K+1);
            Per_sqr(1,(N-1)) = interval;
            K = 0;
            sqrt = 0;
            interval = 0;
        else
            Per_sqr(1,N) = res;
            N = N + 1;
        end
     end
end
figure;
plot(Per_sqr);
size_row = size(T2,1)-1;
K = 0;
N = 1;
interval = 0;
sqrt = 0;
for num_row = 1:size_row
    num_row_2 = num_row + 1;
    res = T2(num_row_2,1)- T2(num_row,1);
    if res <= (1*10^-9)
        K = K + 1;
        sqr(K) = res;
    end   
    if res >= (19*10^-9)
        if K > 0
            for l = 1:K
                for s = 1:l
                    sqrt = sqrt + sqr(s);
                end
                interval = interval + sqrt;
                sqrt = 0;
            end
            interval = (Per_sqr2(1,(N-1))*(K+1) + interval)/(K+1);
            Per_sqr2(1,(N-1)) = interval;
            K = 0;
            sqrt = 0;
            interval = 0;
        else
            Per_sqr2(1,N) = res;
            N = N + 1;
        end
     end
end
figure;
plot(Per_sqr2);

%% ������������� ����������� ������� ����� ���������� ������� � ������
size_row = size(sin_noise,1);
%nonlin_p = zeros(size_row,1);
%nonlin_n = zeros(size_row,1);
%nonlin = zeros(size_row,1);
nonlin_p = zeros(1,size_row);
nonlin_n = zeros(1,size_row);
nonlin = zeros(1,size_row);
J0 = 100*10^-6;
b = 1;
for K = 1:1:size_row
%	nonlin_p(K,1) = J0*(exp(b*real(sin_noise(K,1)))-1);
%	nonlin_n(K,1) = J0*(exp(b*(-1)*real(sin_noise(K,1)))-1);
	nonlin_p(1,K) = J0*(exp(b*real(sin_noise(K,1)))-1);
	nonlin_n(1,K) = J0*(exp(b*(-1)*real(sin_noise(K,1)))-1);
end
nonlin = nonlin_p + nonlin_n;
%%
Fp=3e3; Fs=4e3; eps=0.2;
Wp=2*pi*Fp; Ws=2*pi*Fs;
Rp=10*log10(1+eps^2);
Rs=60;
[n,Wp] = cheb2ord(Wp,Ws,Rp,Rs,'s');
[z,p,k]=cheb2ap(n,Rs);
[b,a]=zp2tf(z,p,k);
f1=75000e3; f2=125000e3;
w1=2*pi*f1; w2=2*pi*f2;
[b1,a1]=lp2bp(b,a,sqrt(w1*w2),(w2-w1));
f=0:1:140e6;
h=freqs(b1,a1,2*pi*f);
plot(f/1000,db(abs(h))),grid,xlabel('f,kHz');
%%
sin_out = filter(b1,a1,nonlin);
