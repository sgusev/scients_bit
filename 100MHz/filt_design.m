% ����� � ��������� ������ ��� ��������� ������ ���������� ������� �
% �������� ��������������� ���

% filter.response = 'lowpass';        % ���
% filter.response = 'arbmag';       %���� ��� ������� �� ������
% filter.response = 'arbmagnphase'; %���� ��� � ��� ������� �� ������
 filter.response = 'bandpass';     %��� - �������-������������ ������
% filter.response = 'bandstop';     %��� - �������-������������� ������
% filter.response = 'notch';        %����������� ������
% filter.response = 'highpass';     %��� - ������ ������ �������                    

filter_types = {'iir','fir'};
d = fdesign.(filter.response);
valid_data = set (d, 'specification');
for num_type = 1:length(filter_types)
    filter_methods = '';
    num_valid_methods = 1;
    filter_type = filter_types(num_type);    
    for i=1:length(valid_data)
        curr_data = char(valid_data(i));
        [splits] = regexp(curr_data,',','split');
        eval_filter_value ='';        
        if strcmp(filter.response, 'arbmag') || strcmp(filter.response, 'arbmagnphase')
            for num_arg=1:length(splits)-2
                eval_filter_value = strcat(eval_filter_value,'1,');
            end;
            eval_filter_value = strcat(eval_filter_value,'[0,1],');
            eval_filter_value = strcat(eval_filter_value,'[0,1]');
        else
            for num_arg=1:length(splits)-1
                eval_filter_value = strcat(eval_filter_value,'1,');
            end;
            eval_filter_value = strcat(eval_filter_value,'1');
        end;
        %d  = fdesign.lowpass(curr_data,1,1,1,1);
        eval_string = strcat('fdesign.(filter.response)(eval(''curr_data''),',eval_filter_value,')');
        eval_string = char(eval_string);
        d = eval(eval_string);
        methods = designmethods(d,char(valid_data(i)),char(filter_type)) ;
        full_methods = designmethods(d,char(valid_data(i)),char(filter_type),'full') ; 
        for num_methods = 1:length(methods)   
            if isempty(strfind(filter_methods, char(methods(num_methods))))
                filter_methods= strcat (filter_methods,'''',char(methods(num_methods)),'''',' % ',char(full_methods(num_methods)),',');    
            end;
        end;
        a=1;
    end;
    switch (char(filter_type))
        case 'iir'
            iir_methods =  filter_methods;
        case 'fir'
            fir_methods =  filter_methods;    
        otherwise  
    end
end;
fprintf('%s\n','���������� ������ ���������� ���:');
disp(char(regexp(iir_methods,',','split')));
fprintf('%s\n','���������� ������ ���������� ���:');
disp(char(regexp(fir_methods,',','split')));
%%
filter.response = 'bandpass';
filter.method = 'cheby2';

d = fdesign.(filter.response);
valid_data = set (d, 'specification');
spec_data = cell(1,1);
num_valid_spec = 1;
for i=1:length(valid_data)
    curr_data = char(valid_data(i));
    [splits] = regexp(curr_data,',','split');
    eval_filter_value ='';
    for num_arg=1:length(splits)-1
        eval_filter_value = strcat(eval_filter_value,'1,');
    end;
    eval_filter_value = strcat(eval_filter_value,'1');    
    eval_string = strcat('fdesign.(filter.response)(eval(''curr_data''),',eval_filter_value,')');
    eval_string = char(eval_string);
    d = eval(eval_string);
    valid_structures = designmethods(d);
    valid = 0;
    num_arg =1;
    while (not (valid) && num_arg<=length(valid_structures))
        valid = strcmp (char(valid_structures(num_arg)), filter.method);
        if (valid)
            spec_data(num_valid_spec,1) = valid_data(i);
            num_valid_spec = num_valid_spec+1;
        end;
        num_arg = num_arg+1;        
    end;
    a=1;
end;
fprintf('%s%c%s%c%s','��� <',filter.response,'> �������. ���������� ������� <',filter.method,'> �������� �� ���������� �����������:');
fprintf('\n');
disp(spec_data);