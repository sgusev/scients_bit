/* Include files */

#include "blascompat32.h"
#include "non_linear_noise_func_sfun.h"
#include "c1_non_linear_noise_func.h"
#include "mwmathutil.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance.chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance.instanceNumber)
#include "non_linear_noise_func_sfun_debug_macros.h"

/* Type Definitions */

/* Named Constants */
#define c1_IN_NO_ACTIVE_CHILD          (0)

/* Variable Declarations */

/* Variable Definitions */
static SFc1_non_linear_noise_funcInstanceStruct chartInstance;

/* Function Declarations */
static void initialize_c1_non_linear_noise_func(void);
static void initialize_params_c1_non_linear_noise_func(void);
static void enable_c1_non_linear_noise_func(void);
static void disable_c1_non_linear_noise_func(void);
static void c1_update_debugger_state_c1_non_linear_noise_func(void);
static const mxArray *get_sim_state_c1_non_linear_noise_func(void);
static void set_sim_state_c1_non_linear_noise_func(const mxArray *c1_st);
static void finalize_c1_non_linear_noise_func(void);
static void sf_c1_non_linear_noise_func(void);
static void init_script_number_translation(uint32_T c1_machineNumber, uint32_T
  c1_chartNumber);
static const mxArray *c1_sf_marshall(void *c1_chartInstance, void *c1_u);
static void c1_info_helper(c1_ResolvedFunctionInfo c1_info[33]);
static const mxArray *c1_emlrt_marshallOut(uint8_T c1_u);
static real_T c1_emlrt_marshallIn(const mxArray *c1_y, char *c1_name);
static uint8_T c1_b_emlrt_marshallIn(const mxArray
  *c1_b_is_active_c1_non_linear_noise_func, char *c1_name);
static void init_io_bus_offset(void);
static void init_dsm_address_info(void);

/* Function Definitions */
static void initialize_c1_non_linear_noise_func(void)
{
  _sfTime_ = (real_T)ssGetT(chartInstance.S);
  chartInstance.c1_is_active_c1_non_linear_noise_func = 0U;
}

static void initialize_params_c1_non_linear_noise_func(void)
{
}

static void enable_c1_non_linear_noise_func(void)
{
  _sfTime_ = (real_T)ssGetT(chartInstance.S);
}

static void disable_c1_non_linear_noise_func(void)
{
  _sfTime_ = (real_T)ssGetT(chartInstance.S);
}

static void c1_update_debugger_state_c1_non_linear_noise_func(void)
{
}

static const mxArray *get_sim_state_c1_non_linear_noise_func(void)
{
  const mxArray *c1_st = NULL;
  const mxArray *c1_y = NULL;
  real_T c1_u;
  const mxArray *c1_b_y = NULL;
  real_T *c1_c_y;
  c1_c_y = (real_T *)ssGetOutputPortSignal(chartInstance.S, 1);
  c1_st = NULL;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_createcellarray(2));
  c1_u = *c1_c_y;
  c1_b_y = NULL;
  sf_mex_assign(&c1_b_y, sf_mex_create("y", &c1_u, 0, 0U, 0U, 0U, 0));
  sf_mex_setcell(c1_y, 0, c1_b_y);
  sf_mex_setcell(c1_y, 1, c1_emlrt_marshallOut
                 (chartInstance.c1_is_active_c1_non_linear_noise_func));
  sf_mex_assign(&c1_st, c1_y);
  return c1_st;
}

static void set_sim_state_c1_non_linear_noise_func(const mxArray *c1_st)
{
  const mxArray *c1_u;
  real_T *c1_y;
  c1_y = (real_T *)ssGetOutputPortSignal(chartInstance.S, 1);
  chartInstance.c1_doneDoubleBufferReInit = true;
  c1_u = sf_mex_dup(c1_st);
  *c1_y = c1_emlrt_marshallIn(sf_mex_dup(sf_mex_getcell(c1_u, 0)), "y");
  chartInstance.c1_is_active_c1_non_linear_noise_func = c1_b_emlrt_marshallIn
    (sf_mex_dup(sf_mex_getcell(c1_u, 1)),
     "is_active_c1_non_linear_noise_func");
  sf_mex_destroy(&c1_u);
  c1_update_debugger_state_c1_non_linear_noise_func();
  sf_mex_destroy(&c1_st);
}

static void finalize_c1_non_linear_noise_func(void)
{
}

static void sf_c1_non_linear_noise_func(void)
{
  uint8_T c1_previousEvent;
  real_T c1_u;
  real_T c1_nargout = 1.0;
  real_T c1_nargin = 1.0;
  real_T c1_b;
  real_T c1_J0;
  real_T c1_y;
  real_T c1_b_b;
  real_T c1_x;
  real_T c1_b_x;
  real_T c1_c_x;
  real_T c1_c_b;
  real_T *c1_b_u;
  real_T *c1_b_y;
  c1_b_y = (real_T *)ssGetOutputPortSignal(chartInstance.S, 1);
  c1_b_u = (real_T *)ssGetInputPortSignal(chartInstance.S, 0);
  _sfTime_ = (real_T)ssGetT(chartInstance.S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG,0);
  _SFD_DATA_RANGE_CHECK(*c1_b_u, 0U);
  _SFD_DATA_RANGE_CHECK(*c1_b_y, 1U);
  c1_previousEvent = _sfEvent_;
  _sfEvent_ = CALL_EVENT;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG,0);
  c1_u = *c1_b_u;
  sf_debug_symbol_scope_push(6U, 0U);
  sf_debug_symbol_scope_add("nargout", &c1_nargout, c1_sf_marshall);
  sf_debug_symbol_scope_add("nargin", &c1_nargin, c1_sf_marshall);
  sf_debug_symbol_scope_add("b", &c1_b, c1_sf_marshall);
  sf_debug_symbol_scope_add("J0", &c1_J0, c1_sf_marshall);
  sf_debug_symbol_scope_add("y", &c1_y, c1_sf_marshall);
  sf_debug_symbol_scope_add("u", &c1_u, c1_sf_marshall);
  CV_EML_FCN(0, 0);

  /* y = u; */
  _SFD_EML_CALL(0,3);
  c1_J0 = 9.9999999999999991E-005;
  _SFD_EML_CALL(0,4);
  c1_b = 1.0;
  _SFD_EML_CALL(0,5);
  c1_b_b = -c1_u;
  c1_x = c1_b_b;
  c1_b_x = c1_x;
  c1_c_x = c1_b_x;
  c1_b_x = c1_c_x;
  c1_b_x = muDoubleScalarExp(c1_b_x);
  c1_c_b = c1_b_x - 1.0;
  c1_y = 9.9999999999999991E-005 * c1_c_b;
  _SFD_EML_CALL(0,-5);
  sf_debug_symbol_scope_pop();
  *c1_b_y = c1_y;
  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG,0);
  _sfEvent_ = c1_previousEvent;
  sf_debug_check_for_state_inconsistency(_non_linear_noise_funcMachineNumber_,
    chartInstance.chartNumber, chartInstance.instanceNumber
    );
}

static void init_script_number_translation(uint32_T c1_machineNumber, uint32_T
  c1_chartNumber)
{
}

static const mxArray *c1_sf_marshall(void *c1_chartInstance, void *c1_u)
{
  const mxArray *c1_y = NULL;
  real_T c1_b_u;
  const mxArray *c1_b_y = NULL;
  c1_y = NULL;
  c1_b_u = *((real_T *)c1_u);
  c1_b_y = NULL;
  sf_mex_assign(&c1_b_y, sf_mex_create("y", &c1_b_u, 0, 0U, 0U, 0U, 0));
  sf_mex_assign(&c1_y, c1_b_y);
  return c1_y;
}

const mxArray *sf_c1_non_linear_noise_func_get_eml_resolved_functions_info(void)
{
  const mxArray *c1_nameCaptureInfo = NULL;
  c1_ResolvedFunctionInfo c1_info[33];
  const mxArray *c1_m0 = NULL;
  int32_T c1_i0;
  c1_ResolvedFunctionInfo *c1_r0;
  c1_nameCaptureInfo = NULL;
  c1_info_helper(c1_info);
  sf_mex_assign(&c1_m0, sf_mex_createstruct("nameCaptureInfo", 1, 33));
  for (c1_i0 = 0; c1_i0 < 33; c1_i0 = c1_i0 + 1) {
    c1_r0 = &c1_info[c1_i0];
    sf_mex_addfield(c1_m0, sf_mex_create("nameCaptureInfo", c1_r0->context, 15,
      0U, 0U, 0U, 2, 1, strlen(c1_r0->context)), "context",
                    "nameCaptureInfo", c1_i0);
    sf_mex_addfield(c1_m0, sf_mex_create("nameCaptureInfo", c1_r0->name, 15, 0U,
      0U, 0U, 2, 1, strlen(c1_r0->name)), "name",
                    "nameCaptureInfo", c1_i0);
    sf_mex_addfield(c1_m0, sf_mex_create("nameCaptureInfo", c1_r0->dominantType,
      15, 0U, 0U, 0U, 2, 1, strlen(c1_r0->dominantType)),
                    "dominantType", "nameCaptureInfo", c1_i0);
    sf_mex_addfield(c1_m0, sf_mex_create("nameCaptureInfo", c1_r0->resolved, 15,
      0U, 0U, 0U, 2, 1, strlen(c1_r0->resolved)), "resolved"
                    , "nameCaptureInfo", c1_i0);
    sf_mex_addfield(c1_m0, sf_mex_create("nameCaptureInfo", &c1_r0->fileLength,
      7, 0U, 0U, 0U, 0), "fileLength", "nameCaptureInfo",
                    c1_i0);
    sf_mex_addfield(c1_m0, sf_mex_create("nameCaptureInfo", &c1_r0->fileTime1, 7,
      0U, 0U, 0U, 0), "fileTime1", "nameCaptureInfo", c1_i0);
    sf_mex_addfield(c1_m0, sf_mex_create("nameCaptureInfo", &c1_r0->fileTime2, 7,
      0U, 0U, 0U, 0), "fileTime2", "nameCaptureInfo", c1_i0);
  }

  sf_mex_assign(&c1_nameCaptureInfo, c1_m0);
  return c1_nameCaptureInfo;
}

static void c1_info_helper(c1_ResolvedFunctionInfo c1_info[33])
{
  c1_info[0].context = "";
  c1_info[0].name = "uminus";
  c1_info[0].dominantType = "double";
  c1_info[0].resolved = "[B]uminus";
  c1_info[0].fileLength = 0U;
  c1_info[0].fileTime1 = 0U;
  c1_info[0].fileTime2 = 0U;
  c1_info[1].context = "";
  c1_info[1].name = "mpower";
  c1_info[1].dominantType = "double";
  c1_info[1].resolved = "[I]$matlabroot$/toolbox/eml/lib/matlab/ops/mpower.m";
  c1_info[1].fileLength = 3623U;
  c1_info[1].fileTime1 = 1227606202U;
  c1_info[1].fileTime2 = 0U;
  c1_info[2].context = "[I]$matlabroot$/toolbox/eml/lib/matlab/ops/mpower.m";
  c1_info[2].name = "nargin";
  c1_info[2].dominantType = "";
  c1_info[2].resolved = "[B]nargin";
  c1_info[2].fileLength = 0U;
  c1_info[2].fileTime1 = 0U;
  c1_info[2].fileTime2 = 0U;
  c1_info[3].context = "[I]$matlabroot$/toolbox/eml/lib/matlab/ops/mpower.m";
  c1_info[3].name = "gt";
  c1_info[3].dominantType = "double";
  c1_info[3].resolved = "[B]gt";
  c1_info[3].fileLength = 0U;
  c1_info[3].fileTime1 = 0U;
  c1_info[3].fileTime2 = 0U;
  c1_info[4].context = "[I]$matlabroot$/toolbox/eml/lib/matlab/ops/mpower.m";
  c1_info[4].name = "isa";
  c1_info[4].dominantType = "double";
  c1_info[4].resolved = "[B]isa";
  c1_info[4].fileLength = 0U;
  c1_info[4].fileTime1 = 0U;
  c1_info[4].fileTime2 = 0U;
  c1_info[5].context = "[I]$matlabroot$/toolbox/eml/lib/matlab/ops/mpower.m";
  c1_info[5].name = "isinteger";
  c1_info[5].dominantType = "double";
  c1_info[5].resolved = "[B]isinteger";
  c1_info[5].fileLength = 0U;
  c1_info[5].fileTime1 = 0U;
  c1_info[5].fileTime2 = 0U;
  c1_info[6].context = "[I]$matlabroot$/toolbox/eml/lib/matlab/ops/mpower.m";
  c1_info[6].name = "isscalar";
  c1_info[6].dominantType = "double";
  c1_info[6].resolved = "[B]isscalar";
  c1_info[6].fileLength = 0U;
  c1_info[6].fileTime1 = 0U;
  c1_info[6].fileTime2 = 0U;
  c1_info[7].context = "[I]$matlabroot$/toolbox/eml/lib/matlab/ops/mpower.m";
  c1_info[7].name = "ndims";
  c1_info[7].dominantType = "double";
  c1_info[7].resolved = "[B]ndims";
  c1_info[7].fileLength = 0U;
  c1_info[7].fileTime1 = 0U;
  c1_info[7].fileTime2 = 0U;
  c1_info[8].context = "[I]$matlabroot$/toolbox/eml/lib/matlab/ops/mpower.m";
  c1_info[8].name = "eq";
  c1_info[8].dominantType = "double";
  c1_info[8].resolved = "[B]eq";
  c1_info[8].fileLength = 0U;
  c1_info[8].fileTime1 = 0U;
  c1_info[8].fileTime2 = 0U;
  c1_info[9].context = "[I]$matlabroot$/toolbox/eml/lib/matlab/ops/mpower.m";
  c1_info[9].name = "size";
  c1_info[9].dominantType = "double";
  c1_info[9].resolved = "[B]size";
  c1_info[9].fileLength = 0U;
  c1_info[9].fileTime1 = 0U;
  c1_info[9].fileTime2 = 0U;
  c1_info[10].context = "[I]$matlabroot$/toolbox/eml/lib/matlab/ops/mpower.m";
  c1_info[10].name = "power";
  c1_info[10].dominantType = "double";
  c1_info[10].resolved = "[I]$matlabroot$/toolbox/eml/lib/matlab/ops/power.m";
  c1_info[10].fileLength = 5380U;
  c1_info[10].fileTime1 = 1227606202U;
  c1_info[10].fileTime2 = 0U;
  c1_info[11].context = "[I]$matlabroot$/toolbox/eml/lib/matlab/ops/power.m";
  c1_info[11].name = "eml_scalar_eg";
  c1_info[11].dominantType = "double";
  c1_info[11].resolved =
    "[I]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m";
  c1_info[11].fileLength = 3524U;
  c1_info[11].fileTime1 = 1227606194U;
  c1_info[11].fileTime2 = 0U;
  c1_info[12].context =
    "[I]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m/any_enums";
  c1_info[12].name = "false";
  c1_info[12].dominantType = "";
  c1_info[12].resolved = "[I]$matlabroot$/toolbox/eml/lib/matlab/elmat/false.m";
  c1_info[12].fileLength = 238U;
  c1_info[12].fileTime1 = 1192459520U;
  c1_info[12].fileTime2 = 0U;
  c1_info[13].context = "[I]$matlabroot$/toolbox/eml/lib/matlab/elmat/false.m";
  c1_info[13].name = "logical";
  c1_info[13].dominantType = "double";
  c1_info[13].resolved = "[B]logical";
  c1_info[13].fileLength = 0U;
  c1_info[13].fileTime1 = 0U;
  c1_info[13].fileTime2 = 0U;
  c1_info[14].context =
    "[I]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m";
  c1_info[14].name = "isstruct";
  c1_info[14].dominantType = "double";
  c1_info[14].resolved = "[B]isstruct";
  c1_info[14].fileLength = 0U;
  c1_info[14].fileTime1 = 0U;
  c1_info[14].fileTime2 = 0U;
  c1_info[15].context =
    "[I]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m/zerosum";
  c1_info[15].name = "class";
  c1_info[15].dominantType = "double";
  c1_info[15].resolved = "[B]class";
  c1_info[15].fileLength = 0U;
  c1_info[15].fileTime1 = 0U;
  c1_info[15].fileTime2 = 0U;
  c1_info[16].context =
    "[I]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m/zerosum";
  c1_info[16].name = "cast";
  c1_info[16].dominantType = "double";
  c1_info[16].resolved =
    "[I]$matlabroot$/toolbox/eml/lib/matlab/datatypes/cast.m";
  c1_info[16].fileLength = 889U;
  c1_info[16].fileTime1 = 1225982242U;
  c1_info[16].fileTime2 = 0U;
  c1_info[17].context =
    "[I]$matlabroot$/toolbox/eml/lib/matlab/datatypes/cast.m";
  c1_info[17].name = "ge";
  c1_info[17].dominantType = "double";
  c1_info[17].resolved = "[B]ge";
  c1_info[17].fileLength = 0U;
  c1_info[17].fileTime1 = 0U;
  c1_info[17].fileTime2 = 0U;
  c1_info[18].context =
    "[I]$matlabroot$/toolbox/eml/lib/matlab/datatypes/cast.m";
  c1_info[18].name = "ischar";
  c1_info[18].dominantType = "char";
  c1_info[18].resolved = "[B]ischar";
  c1_info[18].fileLength = 0U;
  c1_info[18].fileTime1 = 0U;
  c1_info[18].fileTime2 = 0U;
  c1_info[19].context =
    "[I]$matlabroot$/toolbox/eml/lib/matlab/datatypes/cast.m";
  c1_info[19].name = "double";
  c1_info[19].dominantType = "double";
  c1_info[19].resolved = "[B]double";
  c1_info[19].fileLength = 0U;
  c1_info[19].fileTime1 = 0U;
  c1_info[19].fileTime2 = 0U;
  c1_info[20].context =
    "[I]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m/zerosum";
  c1_info[20].name = "plus";
  c1_info[20].dominantType = "double";
  c1_info[20].resolved = "[B]plus";
  c1_info[20].fileLength = 0U;
  c1_info[20].fileTime1 = 0U;
  c1_info[20].fileTime2 = 0U;
  c1_info[21].context =
    "[I]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalar_eg.m/allreal";
  c1_info[21].name = "isreal";
  c1_info[21].dominantType = "double";
  c1_info[21].resolved = "[B]isreal";
  c1_info[21].fileLength = 0U;
  c1_info[21].fileTime1 = 0U;
  c1_info[21].fileTime2 = 0U;
  c1_info[22].context = "[I]$matlabroot$/toolbox/eml/lib/matlab/ops/power.m";
  c1_info[22].name = "eml_scalexp_alloc";
  c1_info[22].dominantType = "double";
  c1_info[22].resolved =
    "[I]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m";
  c1_info[22].fileLength = 794U;
  c1_info[22].fileTime1 = 1227606194U;
  c1_info[22].fileTime2 = 0U;
  c1_info[23].context =
    "[I]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m";
  c1_info[23].name = "minus";
  c1_info[23].dominantType = "double";
  c1_info[23].resolved = "[B]minus";
  c1_info[23].fileLength = 0U;
  c1_info[23].fileTime1 = 0U;
  c1_info[23].fileTime2 = 0U;
  c1_info[24].context =
    "[I]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_alloc.m";
  c1_info[24].name = "not";
  c1_info[24].dominantType = "logical";
  c1_info[24].resolved = "[B]not";
  c1_info[24].fileLength = 0U;
  c1_info[24].fileTime1 = 0U;
  c1_info[24].fileTime2 = 0U;
  c1_info[25].context = "[I]$matlabroot$/toolbox/eml/lib/matlab/ops/power.m";
  c1_info[25].name = "lt";
  c1_info[25].dominantType = "double";
  c1_info[25].resolved = "[B]lt";
  c1_info[25].fileLength = 0U;
  c1_info[25].fileTime1 = 0U;
  c1_info[25].fileTime2 = 0U;
  c1_info[26].context = "[I]$matlabroot$/toolbox/eml/lib/matlab/ops/power.m";
  c1_info[26].name = "eml_scalar_floor";
  c1_info[26].dominantType = "double";
  c1_info[26].resolved =
    "[I]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_floor.m";
  c1_info[26].fileLength = 260U;
  c1_info[26].fileTime1 = 1209323590U;
  c1_info[26].fileTime2 = 0U;
  c1_info[27].context = "[I]$matlabroot$/toolbox/eml/lib/matlab/ops/power.m";
  c1_info[27].name = "ne";
  c1_info[27].dominantType = "double";
  c1_info[27].resolved = "[B]ne";
  c1_info[27].fileLength = 0U;
  c1_info[27].fileTime1 = 0U;
  c1_info[27].fileTime2 = 0U;
  c1_info[28].context = "[I]$matlabroot$/toolbox/eml/lib/matlab/ops/power.m";
  c1_info[28].name = "eml_error";
  c1_info[28].dominantType = "char";
  c1_info[28].resolved =
    "[I]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_error.m";
  c1_info[28].fileLength = 315U;
  c1_info[28].fileTime1 = 1213919546U;
  c1_info[28].fileTime2 = 0U;
  c1_info[29].context = "[I]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_error.m";
  c1_info[29].name = "strcmp";
  c1_info[29].dominantType = "char";
  c1_info[29].resolved = "[B]strcmp";
  c1_info[29].fileLength = 0U;
  c1_info[29].fileTime1 = 0U;
  c1_info[29].fileTime2 = 0U;
  c1_info[30].context = "";
  c1_info[30].name = "mtimes";
  c1_info[30].dominantType = "double";
  c1_info[30].resolved = "[I]$matlabroot$/toolbox/eml/lib/matlab/ops/mtimes.m";
  c1_info[30].fileLength = 2408U;
  c1_info[30].fileTime1 = 1227606202U;
  c1_info[30].fileTime2 = 0U;
  c1_info[31].context = "";
  c1_info[31].name = "exp";
  c1_info[31].dominantType = "double";
  c1_info[31].resolved = "[I]$matlabroot$/toolbox/eml/lib/matlab/elfun/exp.m";
  c1_info[31].fileLength = 324U;
  c1_info[31].fileTime1 = 1203440820U;
  c1_info[31].fileTime2 = 0U;
  c1_info[32].context = "[I]$matlabroot$/toolbox/eml/lib/matlab/elfun/exp.m";
  c1_info[32].name = "eml_scalar_exp";
  c1_info[32].dominantType = "double";
  c1_info[32].resolved =
    "[I]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_exp.m";
  c1_info[32].fileLength = 243U;
  c1_info[32].fileTime1 = 1203440792U;
  c1_info[32].fileTime2 = 0U;
}

static const mxArray *c1_emlrt_marshallOut(uint8_T c1_u)
{
  const mxArray *c1_y = NULL;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 3, 0U, 0U, 0U, 0));
  return c1_y;
}

static real_T c1_emlrt_marshallIn(const mxArray *c1_y, char *c1_name)
{
  real_T c1_b_y;
  real_T c1_d0;
  sf_mex_import(c1_name, sf_mex_dup(c1_y), &c1_d0, 1, 0, 0U, 0, 0U, 0);
  c1_b_y = c1_d0;
  sf_mex_destroy(&c1_y);
  return c1_b_y;
}

static uint8_T c1_b_emlrt_marshallIn(const mxArray
  *c1_b_is_active_c1_non_linear_noise_func, char *c1_name)
{
  uint8_T c1_y;
  uint8_T c1_u0;
  sf_mex_import(c1_name, sf_mex_dup(c1_b_is_active_c1_non_linear_noise_func),
                &c1_u0, 1, 3, 0U, 0, 0U, 0);
  c1_y = c1_u0;
  sf_mex_destroy(&c1_b_is_active_c1_non_linear_noise_func);
  return c1_y;
}

static void init_io_bus_offset(void)
{
}

static void init_dsm_address_info(void)
{
}

/* SFunction Glue Code */
void sf_c1_non_linear_noise_func_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(3363862691U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(745827519U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(2717021099U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(3762582962U);
}

mxArray *sf_c1_non_linear_noise_func_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1,1,4,
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateDoubleMatrix(4,1,mxREAL);
    double *pr = mxGetPr(mxChecksum);
    pr[0] = (double)(3872271136U);
    pr[1] = (double)(1042433737U);
    pr[2] = (double)(2031834343U);
    pr[3] = (double)(728248087U);
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  return(mxAutoinheritanceInfo);
}

static mxArray *sf_get_sim_state_info_c1_non_linear_noise_func(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  char *infoEncStr[] = {
    "100 S1x2'type','srcId','name','auxInfo'{{M[1],M[5],T\"y\",},{M[8],M[0],T\"is_active_c1_non_linear_noise_func\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 2, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c1_non_linear_noise_func_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (_non_linear_noise_funcMachineNumber_,
           1,
           1,
           1,
           2,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance.chartNumber),
           &(chartInstance.instanceNumber),
           ssGetPath(S),
           (void *)S);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          init_script_number_translation(_non_linear_noise_funcMachineNumber_,
            chartInstance.chartNumber);
          sf_debug_set_chart_disable_implicit_casting
            (_non_linear_noise_funcMachineNumber_,chartInstance.chartNumber,1);
          sf_debug_set_chart_event_thresholds
            (_non_linear_noise_funcMachineNumber_,
             chartInstance.chartNumber,
             0,
             0,
             0);
          _SFD_SET_DATA_PROPS(0,1,1,0,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,"u",0,
                              c1_sf_marshall);
          _SFD_SET_DATA_PROPS(1,2,0,1,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,"y",0,
                              NULL);
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of EML Model Coverage */
        _SFD_CV_INIT_EML(0,1,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,73);
        _SFD_TRANS_COV_WTS(0,0,0,1,0);
        if (chartAlreadyPresent==0) {
          _SFD_TRANS_COV_MAPS(0,
                              0,NULL,NULL,
                              0,NULL,NULL,
                              1,NULL,NULL,
                              0,NULL,NULL);
        }

        {
          real_T *c1_u;
          real_T *c1_y;
          c1_y = (real_T *)ssGetOutputPortSignal(chartInstance.S, 1);
          c1_u = (real_T *)ssGetInputPortSignal(chartInstance.S, 0);
          _SFD_SET_DATA_VALUE_PTR(0U, c1_u);
          _SFD_SET_DATA_VALUE_PTR(1U, c1_y);
        }
      }
    } else {
      sf_debug_reset_current_state_configuration
        (_non_linear_noise_funcMachineNumber_,chartInstance.chartNumber,
         chartInstance.instanceNumber);
    }
  }
}

static void sf_opaque_initialize_c1_non_linear_noise_func(void *chartInstanceVar)
{
  chart_debug_initialization(chartInstance.S,0);
  initialize_params_c1_non_linear_noise_func();
  initialize_c1_non_linear_noise_func();
}

static void sf_opaque_enable_c1_non_linear_noise_func(void *chartInstanceVar)
{
  enable_c1_non_linear_noise_func();
}

static void sf_opaque_disable_c1_non_linear_noise_func(void *chartInstanceVar)
{
  disable_c1_non_linear_noise_func();
}

static void sf_opaque_gateway_c1_non_linear_noise_func(void *chartInstanceVar)
{
  sf_c1_non_linear_noise_func();
}

static mxArray* sf_opaque_get_sim_state_c1_non_linear_noise_func(void
  *chartInstanceVar)
{
  mxArray *st = (mxArray *) get_sim_state_c1_non_linear_noise_func();
  return st;
}

static void sf_opaque_set_sim_state_c1_non_linear_noise_func(void
  *chartInstanceVar, const mxArray *st)
{
  set_sim_state_c1_non_linear_noise_func(sf_mex_dup(st));
}

static void sf_opaque_terminate_c1_non_linear_noise_func(void *chartInstanceVar)
{
  if (sim_mode_is_rtw_gen(chartInstance.S) || sim_mode_is_external
      (chartInstance.S)) {
    sf_clear_rtw_identifier(chartInstance.S);
  }

  finalize_c1_non_linear_noise_func();
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c1_non_linear_noise_func(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    initialize_params_c1_non_linear_noise_func();
  }
}

static void mdlSetWorkWidths_c1_non_linear_noise_func(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable("non_linear_noise_func",
      "non_linear_noise_func",1);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop("non_linear_noise_func",
                "non_linear_noise_func",1,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop("non_linear_noise_func",
      "non_linear_noise_func",1,"gatewayCannotBeInlinedMultipleTimes"));
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,"non_linear_noise_func",
        "non_linear_noise_func",1,1);
      sf_mark_chart_reusable_outputs(S,"non_linear_noise_func",
        "non_linear_noise_func",1,1);
    }

    sf_set_rtw_dwork_info(S,"non_linear_noise_func","non_linear_noise_func",1);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
    ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  }

  ssSetChecksum0(S,(293105290U));
  ssSetChecksum1(S,(4110516717U));
  ssSetChecksum2(S,(884100450U));
  ssSetChecksum3(S,(3257008030U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
}

static void mdlRTW_c1_non_linear_noise_func(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    sf_write_symbol_mapping(S, "non_linear_noise_func", "non_linear_noise_func",
      1);
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c1_non_linear_noise_func(SimStruct *S)
{
  chartInstance.chartInfo.chartInstance = NULL;
  chartInstance.chartInfo.isEMLChart = 1;
  chartInstance.chartInfo.chartInitialized = 0;
  chartInstance.chartInfo.sFunctionGateway =
    sf_opaque_gateway_c1_non_linear_noise_func;
  chartInstance.chartInfo.initializeChart =
    sf_opaque_initialize_c1_non_linear_noise_func;
  chartInstance.chartInfo.terminateChart =
    sf_opaque_terminate_c1_non_linear_noise_func;
  chartInstance.chartInfo.enableChart =
    sf_opaque_enable_c1_non_linear_noise_func;
  chartInstance.chartInfo.disableChart =
    sf_opaque_disable_c1_non_linear_noise_func;
  chartInstance.chartInfo.getSimState =
    sf_opaque_get_sim_state_c1_non_linear_noise_func;
  chartInstance.chartInfo.setSimState =
    sf_opaque_set_sim_state_c1_non_linear_noise_func;
  chartInstance.chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c1_non_linear_noise_func;
  chartInstance.chartInfo.zeroCrossings = NULL;
  chartInstance.chartInfo.outputs = NULL;
  chartInstance.chartInfo.derivatives = NULL;
  chartInstance.chartInfo.mdlRTW = mdlRTW_c1_non_linear_noise_func;
  chartInstance.chartInfo.mdlStart = mdlStart_c1_non_linear_noise_func;
  chartInstance.chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c1_non_linear_noise_func;
  chartInstance.chartInfo.extModeExec = NULL;
  chartInstance.chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance.chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance.chartInfo.storeCurrentConfiguration = NULL;
  chartInstance.S = S;
  ssSetUserData(S,(void *)(&(chartInstance.chartInfo)));/* register the chart instance with simstruct */
  if (!sim_mode_is_rtw_gen(S)) {
    init_dsm_address_info();
  }

  chart_debug_initialization(S,1);
}

void c1_non_linear_noise_func_method_dispatcher(SimStruct *S, int_T method, void
  *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c1_non_linear_noise_func(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c1_non_linear_noise_func(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c1_non_linear_noise_func(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c1_non_linear_noise_func_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
