start = 4000000;%������ ������� �������(��)
stop = 6000000;%����� ������� �������(��)
freq = 5000000;
step = 100; %��� �������
N_start = start/step;
N_stop = stop/step;
[M] = textread('Spec.USR','','delimiter',',');
%figure();
%plot(M(:,1),M(:,3))

for N = N_start:N_stop
    K = N - N_start + 1;
    O(K,1) = M(N,1);
	O(K,2) = M(N,3);
end
%figure();
%plot(O(:,1),O(:,2));

%����������� ������� ������� � Dbc
max_O = max(O(:,2));
size_O = size(O,1);
for N = 1:size_O;
	Odb(N,1) = O(N,1);
    if O(N,2) ~= max_O
        Odb(N,2) = 20*log10((O(N,2))/max_O) - 10*log10(step);
    else
        Odb(N,2) = 20*log10((O(N,2))/max_O);
        max_Odb = Odb(N,2);
    end
end
figure();
grid on;
plot(Odb(:,1),Odb(:,2),'.')
title('5Mhz out Dbc');
xlabel('frequency,Hz');
ylabel('spectrum,Dbc');

%������������ ��������
S = 0;
size_Odb = size(Odb,1);
l = 0;
u = 0;
p = 1;
f(p) = step;
for N_2 = 2:size_Odb;
    N_1 = N_2 - 1;
    if Odb(N_1,2) == max_Odb
        l = 1;
        max_freq = Odb(N_1,1);
    end
    if u == 1
        a(p,1) = (Odb(N_2,2) - Odb(N_1,2))/(log10(Odb(N_2,1) - max_freq) - log10(Odb(N_1,1) - max_freq));
        b(p) = Odb(N_1,2);
        f(p) = Odb(N_1,1) - max_freq;
        p = p + 1;
    end
    if l == 1
        u = 1;
    end 
end
num_size = size(a,1) - 1
J = 0;
for N = 1:num_size
    J = J + ( (10^(b(N)/10)) * (f(N)^(-1*a(N,1)/10)) * (((a(N,1)/10)+1)^(-1)) * ((f(N+1)^((a(N,1)/10)+1)) - (f(N)^((a(N,1)/10)+1))) );
    Q(N) = J;
end
Djitter = 1/(2*pi*freq)*sqrt(2*J);