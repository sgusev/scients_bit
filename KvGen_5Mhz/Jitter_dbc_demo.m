start_freq = 1000;
stop_freq = 10000;
db = -150;
f0 = 100000000;
A =db + 10*log10(stop_freq-start_freq);
W = sqrt(2*10^(A/10));
t = W/(2*pi*f0)