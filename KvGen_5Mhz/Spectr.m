%#######################
%������ ������ �� �����,
%���������� ����� ����� ��������,
%�������� ������
%#######################

time_from = 6*10^-3;
time_to = 16*10^-3;
[M] = textread('5MHz_out_3.USR','','delimiter',',');
number_from = round(time_from/M(2,1));
number_to = size(M,1);
Fd = 1/(M(2,1)); % ������� �������������

%% ��������� ������ ����� �������� � ������ � ������
for num_row = number_from:1:number_to
    num_row_2 = num_row + 1;
    if M(num_row,2) >= 0
       if M(num_row_2,2) < 0
            number_from = num_row
            break
       end 
    end
%    num_row = num_row + 1;
end
num_row = 0;
num_row_2 = 0;
for num_row = number_to:-1:number_from
    num_row_2 = num_row - 1;
    if M(num_row,2) <= 0
       if M(num_row_2,2) > 0
            number_to = num_row_2
            break
       end 
   end   
end
%Signal_tmp = M';
%Signal = Signal_tmp(2,number_from:number_to);
Signal = M;

%% ������ �������� �������
size_row = size(Signal,2)-1; %����� ���������
step = 0.001*10^-6; %������� ��������� �������

N = 1;
%time = 0;
%num_row_2 = 1;
for num_row = 1:size_row
    num_row_2 = num_row + 1;
    size_row - num_row
    if Signal(num_row,2) >= 0
       if Signal(num_row_2,2) < 0
           K = abs(Signal(num_row,2)/Signal(num_row_2,2));
           P = Signal(num_row,1) + step*K/(1+K);
           T(N,1) = P;
           N = N + 1;
       end 
%   time = time + step;
   end 
end

size_row = size(T,1)-1;
size_row_strt = size_row - 1000;
num_row_2 = 1;
%Per_sqr = zeros(1,size_row);
Per_sqr = zeros(1,1000);

%for num_row = size_row:-1:size_row_strt
%for num_row = 1:size_row
for num_row = 1:1000
    num_row
%    size_row - num_row
    num_row_2 = num_row + 1;
    Per_sqr(1,num_row) = T(num_row_2,1)- T(num_row,1);
end
figure;
grid on;
plot(Per_sqr);

%% ������ � ���������� �������
%FftL=1048576;% ���������� ����� ����� �������
FftL=(number_to - number_from);% ���������� ����� ����� �������
FftS=abs(fft(Signal(:,2),FftL));% ��������� �������������� ����� �������
FftS=2*FftS./FftL;% ���������� ������� �� ���������
FftS(1)=FftS(1)/2;% ���������� ���������� ������������ � �������
F=0:Fd/FftL:Fd/2-Fd/FftL;% ������ ������ ������������ ������� �����
figure% ������� ����� ����
plot(F,FftS(1:length(F)),'.');% ���������� ������� ����� �������
title('������');% ������� �������

%% ������ � ���������� ������� � dbc
step = round(1/(time_to - time_from));
max_FftS = max(FftS);
size_FftS = size(FftS,2);
FftS_dbc = zeros(1,size_FftS);
for N = 1:size_FftS;
    if FftS(N) ~= max_FftS
        FftS_dbc(N) = 20*log10((FftS(N))/max_FftS) - 10*log10(step);
    else
        FftS_dbc(N) = 20*log10((FftS(N))/max_FftS);
        max_FftS_dbc = FftS_dbc(N);
    end
end
figure% ������� ����� ����
plot(F,FftS_dbc(1:length(F)),'.');
title('������, ���');% ������� �������